///string_to_array(string,separator?)

var str = argument[0];
if argument_count >= 2
	var seperator = argument[1]
else
	var seperator = "|";
var array = [];
var amount = string_count(seperator,str)+1;

for(var i = 0; i != amount; i += 1)
{
	var index = string_pos(seperator,str);
	if (index != 0)
	{
		array[i] = string_copy(str,0,index-1);
		str = string_copy(str,index+1,string_length(str))
	}
	else
	{
		array[i] = str; break;
	}
	
	console_log(i," ",array[i],": ",str)
}

return array;