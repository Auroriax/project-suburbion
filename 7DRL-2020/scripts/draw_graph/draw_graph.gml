var data = argument[0];
var hi = argument[1]
var xxo = argument[2];
var yyo = argument[3];
var width = argument[4];
var height = argument[5];

var mr = 10; if argument_count > 6 {mr = argument[6]}
var clr = c_black; if argument_count > 7 {clr = argument[7]}
draw_set_alpha(0.5)
draw_rectangle_color(xxo,yyo,xxo+width,yyo+height,clr,clr,clr,clr,false)
draw_set_alpha(1)

var highestdatapoint = hi;

var j = 0;
var prevx = xxo; var prevy = yyo + height;
for(var i = max(0,array_length_1d(data) - mr); i != array_length_1d(data); i += 1)
{
	draw_set_color(c_black)
	var xx = xxo+(width*((j+0.5)/mr));
	var yy = yyo+height-(height*(clamp(data[i],0,hi)/highestdatapoint));
	
	if data[i] != -1
	{
		var wdt = 1; if i == array_length_1d(data)-1 {wdt = 2}
	
		if i != 0 && data[i-1] != -1 && j != 0 
		{
			draw_line_width(prevx,prevy,xx,yy,wdt)
		}
	
		var sprt = s_donut; 
		draw_sprite(sprt,0,xx-1,yy-3)
		
		draw_set_font(f_bodytextsmaller)
		draw_set_halign(1)
		var yyy = -15; if even(j) {yyy = +25}
		draw_textoutline(xx+3,yy-yyy,string(data[i]),c_black)
		draw_set_color(c_white);
		draw_text(xx+3,yy-yyy,string(data[i]))
	}
	
	prevx = xx; prevy = yy;
	
	j += 1;
}

//draw_sprite(sprite_index,image_index,xxo,yyo-26)

//draw_text(xxo+70-25,yyo-30,string(data[9]))

//draw_sprite(s_trophy,0,xxo+width-50,yyo-26)
//draw_set_halign(0); draw_set_valign(0);
//draw_text(xxo+width-35,yyo-30,string(hi))