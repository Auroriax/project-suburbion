//console_log("Drawing line: ",i)
var i = argument[0];

connection = connections[i];

if (connection.revealed = false) {exit;}

var station1 = nodes[connection.stationin];
var station2 = nodes[connection.stationout];
	
var hovered = false;
if (hoverednode != -1 && (station1 == nodes[hoverednode] || station2 == nodes[hoverednode]))
	{draw_set_color(c_white); hovered = true;}
else
	draw_set_color(make_color_hsv(0,0,255 * (0.25 + (0.5 * (connection.frequency/60)))))
draw_line_width(xoff + xdist * station1.gridx, yoff + ydist * station1.gridy, xoff + xdist * station2.gridx, yoff + ydist * station2.gridy, min(connection.traveltime,15))
	
var avgx = (station1.gridx + station2.gridx) * 0.5;
var avgy = (station1.gridy + station2.gridy) * 0.5;

if hovered
{
	//Rotating circle
	if hoverednode != currentnode
	draw_sprite_ext(s_orb,0,xoff + xdist * nodes[hoverednode].gridx, yoff + ydist * nodes[hoverednode].gridy,1,1, (buildup / 60) * 360, -1,1)
	
	var currentConnection = (currentnode == connection.stationin || currentnode == connection.stationout);
	//Animated circle
	if currentConnection && hoverednode != currentnode {
		

		//Red circle
		draw_set_color(c_red);
		var dist = 0.1 + (buildup / 60) * 0.9;
		if heldtimer > 0 {dist = 0.1 + (heldtimer/heldtimermax) * 0.9}
		
		var alph = 1;
		if dist < 0.3 {alph = (dist - 0.1) * 5}
		draw_set_alpha(alph);
		
		var diffx = (station1.gridx - station2.gridx) * dist;
		var diffy = (station1.gridy - station2.gridy) * dist;
		if (currentnode == connection.stationin)
		{
			diffx = -diffx; diffy = -diffy
			draw_circle(xoff + xdist * (station1.gridx + diffx),yoff + ydist * (station1.gridy + diffy),20,false)
		}
		else
		{
			draw_circle(xoff + xdist * (station2.gridx + diffx),yoff + ydist * (station2.gridy + diffy),20,false)
		}
		draw_set_alpha(1);
	}
	
	//Time left labels
	if hovereditem != -1
	{draw_set_alpha(0.5)}
	var timeleft = connection.frequency - (time + connection.start) % connection.frequency;
	if (timeleft == connection.frequency) {timeleft = 0};
	var txt = string(timeleft) + " +"+string(connection.traveltime);
	var c = c_black
	if timeleft == 0 && blink && currentConnection {c = c_gray}
	
	draw_set_font(f_bodytext)
	var lh = 0;
	lh += draw_textlabel(xoff + xdist * avgx, yoff + ydist * avgy + lh, txt, c, c_white);
	lh -= 8;
	
	draw_set_color(c_white);
	var txt = "Next: " + string(connection.frequency + timeleft);
	draw_set_font(f_bodytextsmaller); draw_set_halign(1);
	draw_textoutline(xoff + xdist * avgx, yoff + ydist * avgy + lh, txt, c_black, c_white);
	draw_text(xoff + xdist * avgx, yoff + ydist * avgy + lh, txt)
	draw_set_alpha(1);  draw_set_halign(0);
}