/// @description  draw_pie(x ,y ,value, max, colour, radius, transparency, startangle)
/// @param x 
/// @param y 
/// @param value
/// @param  max
/// @param  colour
/// @param  radius
/// @param  transparency
/// @param startangle
///Source: http://www.davetech.co.uk/gamemakercircularhealthbars

if (argument[2] > 0) { // no point even running if there is nothing to display (also stops /0
    var i, len, tx, ty, val;
    
    var numberofsections = 60 // there is no draw_get_circle_precision() else I would use that here
    var sizeofsection = 360/numberofsections
    
    val = (argument[2]/argument[3]) * numberofsections 
	
	var prevalph = draw_get_alpha();
    
    if (val > 1) { // HTML5 version doesnt like triangle with only 2 sides 
    
        draw_set_colour(argument[4]);
        draw_set_alpha(argument[6]);
        
        draw_primitive_begin(pr_trianglefan);
        draw_vertex(argument[0], argument[1]);
        
        for(i=0; i<=val; i++) {
			var startangle = 90;
			if argument_count >= 8 {
				startangle = argument[7]
			}
            len = (i*sizeofsection)+startangle;
            tx = lengthdir_x(argument[5], len);
            ty = lengthdir_y(argument[5], len);
            draw_vertex(argument[0]+tx, argument[1]+ty);
        }
        draw_primitive_end();
        
    }
    draw_set_alpha(prevalph);
}