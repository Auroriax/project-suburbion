var str = ""

var rand = random(1);
var sufrand = random(1);

var prefixes = ["Ga", "Do", "Na", "Rak", "Bu", "Nin", "Yun", "Go", "Zun", "Sche",
"Bre", "D'n ", "End", "Ro", "Ter", "Arn"]
var bases = ["nu", "dam", "ven", "ka", "du", "ijs", "ter", "en", "lo", "tog", 
"hem", "tel", "sen"]
var suffixes = ["-dijk", "-stede", "-loft", "-doko", "-dyke", "-softa", "-recht",
"-tje", "-ray", "-recht", "-hoven", "-bosch", "-haag", "-bommel", "-borg", "-daal"]

str += array_random(prefixes)
if rand > 0.1
str += array_random(bases)
if rand > 0.5
str += array_random(bases)
if rand > 0.9
str += array_random(bases)

if sufrand > 0.8
str += array_random(suffixes)

return str;