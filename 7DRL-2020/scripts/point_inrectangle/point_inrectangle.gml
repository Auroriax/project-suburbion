///point_inrectangle(in_x,in_y, left, right, top, bottom
///@param in_x
///@param in_y
///@param left
///@param top
///@param right
///@param bottom
///@param draw?

var inx = argument[0];
var iny = argument[1];
var left = argument[2];
var top = argument[3];
var right = argument[4];
var bottom = argument[5];

var in = point_in_rectangle(inx,iny,left,top,right,bottom)
	
if in && argument_count >= 7 && argument[6] == true
{
	var c = c_white;
	draw_rectangle_color(left,top,right,bottom,c,c,c,c,true);
}

return (in)