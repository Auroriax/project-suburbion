///item_sell(amount)

var itemID = argument[0];
var start = argument[1];
var curnode = argument[2];

for (i = start; i != 0; i -= 1)
{
	if inventory[itemID] >= i
	{
		curnode.stock[itemID] += i;
		money += curnode.prices[itemID] * i;
		inventory[itemID] -= i;
		
		if i > 1
			var txt = "x "+string(i)+" +€"+string(curnode.prices[itemID] * i)
		else
			var txt = "+€"+string(curnode.prices[itemID] * i);
		floattext(mouse_x,mouse_y - 10,txt);
		for(var j = 0; j != i; j += 1)
		{
			if transferred[itemID] > 0
				time -= 1;
			transferred[itemID] -= 1;
		}
		
		audio(a_coin,1.5 - min(0.05 * i, 0.5) + random(0.1),0.3)
		if money > goal
		{
			gameover();
		}
		return(true)
	}
}

return(false);