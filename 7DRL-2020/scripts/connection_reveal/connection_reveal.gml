///connection_reveal(connectionID)

var index = connections[argument[0]];
index.revealed = true;

nodes[index.stationin].revealed = true;
nodes[index.stationout].revealed = true;