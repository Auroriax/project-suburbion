///draw_text_get_height(x,y,txt,separation,overflowWidth), return width

var xx = argument[0]
var yy = argument[1]
var txt = argument[2]
if argument_count >= 4
	var sep = argument[3]
else
	var sep = -1;
if argument_count >= 5
	var overflow = argument[4]
else
	var overflow = -1;

draw_text_ext(xx,yy,txt,sep,overflow)

return round(string_height_ext(txt,sep,overflow))