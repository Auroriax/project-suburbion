///draw_textlabel(x,y,txt,textcolor,labelcolor)

var labelx = argument[0];
var labely = argument[1];
var txt = argument[2];
var textcolor = argument[3];
var labelcolor = argument[4]

var lw = string_width(txt) + 2; var lh = string_height(txt);
var c = labelcolor;
draw_rectangle_color(labelx - lw * 0.5, labely - lh * 0.5, labelx + lw * 0.5, labely + lh * 0.5,c,c,c,c,false)

draw_set_halign(1); draw_set_valign(1);
var c = textcolor;
draw_text_color(labelx, labely, txt, c, c, c, c, 1);

draw_set_halign(0);
draw_set_valign(0);

return lh; //Returns height