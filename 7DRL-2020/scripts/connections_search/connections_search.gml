var grid = argument[0];
var z = argument[1]
var interval = argument[2];
var connectionsAllowed = argument[3];

var node = nodes[z]
var xx = node.gridx;
var yy = node.gridy;

for(var c = 0; c != 4; c += 1)
{
	switch (c) { //Can only make connections in one direction
		case 0: var i = interval; var j = 0; var diagonalrule = false; break; //East
		case 1: var i =  0; var j = interval; var diagonalrule = false; break; //South
		case 2: var i = -interval; var j = interval; var diagonalrule = true; break; //Southwest
		case 3: var i =  interval; var j = interval; var diagonalrule = true; break; //Southeast
		default: show_error("Could not check corners correctly.",true)
	}
	
	if diagonalrule && interval != 1 {diagonalrule = false;}

	if point_inrectangle(xx+i,yy+j,0,0,w-1,h-1) && grid[xx + i, yy + j] >= 0
	{
		var othernode = nodes[grid[xx + i, yy + j]];
		var legit = false;
		switch (c) { //Can only make connections in one direction
			case 0: if !node.e && !othernode.w 
				{node.e = true; othernode.w = true; legit = true;} break;
			case 1: if !node.s && !othernode.n 
				{node.s = true; othernode.n = true; legit = true;} break;
			case 2: if !node.sw && !othernode.ne 
				{node.sw = true; othernode.ne = true; legit = true;} break;
			case 3: if !node.se && !othernode.nw 
				{node.se = true; othernode.nw = true; legit = true;} break;
		}
		
		if (legit && diagonalrule)
		{
			with(o_staticConnection)
			{
				if legit
				{
					var nodein = other.nodes[stationin];
					var nodeout = other.nodes[stationout];
					
					if ((nodein.gridx == xx + i && nodein.gridy == yy && nodeout.gridx == xx && nodeout.gridy == yy + j) ||
					(nodeout.gridx == xx + i && nodeout.gridy == yy && nodein.gridx == xx && nodein.gridy == yy + j))
					{
						legit = false;
						//console_log("Diagonal Rule applied");
					}
				}
			}
		}
		
		if (legit)
		{
			var con = connection_define(z, grid[xx + i, yy + j], 1 + round((interval - 1) * 0.5),interval);
			node.localconnections = array_push(node.localconnections,con);
			othernode.localconnections = array_push(othernode.localconnections,con);
			connectionsAllowed[c] = false;
		}
	}
}

return(connectionsAllowed)