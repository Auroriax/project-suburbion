///node_define(id)

var index = argument[0];

nodes[index] = instance_create_layer(0,0,"Instances",o_staticNode);
var ins = nodes[index];

ins.gridx = argument[1];
ins.gridy = argument[2];

ins.name = name_generate();
ins.poi = subtitle_generate();

return ins;