///array_to_string(array,separator?)

var array = argument[0];
if argument_count >= 2
	var seperator = argument[1]
else
	var seperator = "|";
var txt = "";

for(var i = 0; i != array_length_1d(array); i += 1)
{
	txt += string(array[i]);
	if i+1 != array_length_1d(array) 
		{txt += seperator}
}

return txt;