///connection_define(station1,station2,length,traveltimemultiplier, length)

var index = array_length_1d(connections);

connections[index] = instance_create_layer(0,0,"Instances",o_staticConnection);
var ins = connections[index];

ins.stationin = argument[0];
ins.stationout = argument[1];

ins.traveltime = irandom_range(5,15) * argument[2];
ins.frequency = choose(10,15,15,20,20,20,20,30);
ins.start = irandom(30);

ins.length = argument[3];

return index;