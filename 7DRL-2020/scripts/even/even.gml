///even(int)
//True if a number is even (0, 2, 4, etc.), false when a number is odd (-1,7,13, etc.)

return(argument0/2 == floor(argument0/2))