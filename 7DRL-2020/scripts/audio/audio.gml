///audio(id,pitch,gain?)

var a = audio_play_sound(argument[0],-1,false)
audio_sound_pitch(a,argument[1])

if argument_count >= 3
audio_sound_gain(a,argument[2],0)

//console_log("Playing audio: ",a)

return(a);