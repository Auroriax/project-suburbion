/// @description drawtxtoutline(x,y,string[,outercolor, width, alpha, separation, multiplier,rotation])
/// @param x
/// @param y
/// @param string
/// @param outercolor*
/// @param width*
/// @param alpha*
/// @param separation*
/// @param multiplier*
/// @param rotation*
//Make sure to call this BEFORE you draw the text you want to outline!

var clr = draw_get_colour()
var wdt = -1; var sep = -1; var alph = draw_get_alpha(); 
var mpl = 1; var xsc = 1; var ysc = 1; var rot = 0;
if argument_count >= 4 {clr = argument[3]}
if argument_count >= 5 {wdt = argument[4]}
if argument_count >= 6 {alph = argument[5]}
if argument_count >= 7 {sep = argument[6]}
if argument_count >= 8 {mpl = argument[7]}
if argument_count >= 9 {rot = argument[8]}
draw_text_ext_transformed_color(argument[0]+mpl,argument[1] +mpl,argument[2],sep,wdt,xsc,ysc,rot,clr,clr,clr,clr,alph)
draw_text_ext_transformed_color(argument[0]+mpl,argument[1] -mpl,argument[2],sep,wdt,xsc,ysc,rot,clr,clr,clr,clr,alph)
draw_text_ext_transformed_color(argument[0]-mpl,argument[1] +mpl,argument[2],sep,wdt,xsc,ysc,rot,clr,clr,clr,clr,alph)
draw_text_ext_transformed_color(argument[0]-mpl,argument[1] -mpl,argument[2],sep,wdt,xsc,ysc,rot,clr,clr,clr,clr,alph)

draw_text_ext_transformed_color(argument[0],argument[1] -mpl,argument[2],sep,wdt,xsc,ysc,rot,clr,clr,clr,clr,alph)
draw_text_ext_transformed_color(argument[0] -mpl-1,argument[1],argument[2],sep,wdt,xsc,ysc,rot,clr,clr,clr,clr,alph)
draw_text_ext_transformed_color(argument[0],argument[1] +mpl,argument[2],sep,wdt,xsc,ysc,rot,clr,clr,clr,clr,alph)
draw_text_ext_transformed_color(argument[0] +mpl+1,argument[1],argument[2],sep,wdt,xsc,ysc,rot,clr,clr,clr,clr,alph)