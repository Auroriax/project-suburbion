///draw_tooltip(border,width,height,title,description)

draw_set_valign(0); draw_set_halign(0);

var border = argument[0];
var tx = mouse_x + border; 
var ty = mouse_y + border; 
var width = argument[1]; 
var height = argument[2];
if (tx + width >= room_width) {tx -= width + border * 2}
if (ty + height >= room_height){ty -= height + border * 2}
var c = c_gray; draw_set_alpha(0.95);
draw_rectangle_color(tx,ty,tx+width,ty+height,c,c,c,c,false)
var c = c_white; draw_set_alpha(1);
draw_rectangle_color(tx-2,ty - 2,tx+width + 2,ty+height + 2,c,c,c,c,true);

var dx = tx + border; var dy = ty + border;
draw_set_font(f_bodytextbold); draw_set_color(c_black)
dy += draw_text_get_height(dx,dy,argument[3]) + 5;

draw_set_font(f_bodytext)
dy += draw_text_get_height(dx,dy,argument[4],-1, width - border * 2) + 15;

draw_graph(netvaluegraph, goal, dx,dy+20, width - border * 2, 150)