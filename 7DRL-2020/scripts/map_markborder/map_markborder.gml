///map_markborder(DSgridIndex, x, y)

var grid = argument[0];
var xx = argument[1];
var yy = argument[2];

for(var hor = -1; hor <= 1; hor += 1)
{
	for(var ver = -1; ver <= 1; ver += 1)
	{
		if point_inrectangle(xx+hor,yy+ver,0,0,w-1,h-1) && grid[xx + hor, yy + ver] == MAP_EMPTY
		{
			grid[xx + hor, yy + ver] = MAP_BORDER;
		}
	}
}

return(grid)