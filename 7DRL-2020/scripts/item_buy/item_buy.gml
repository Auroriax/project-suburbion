///item_buy(amount, itemcount)

var itemID = argument[0];
var start = argument[1];
var curnode = argument[2];
var itemcount = argument[3];

for (i = start; i != 0; i -= 1)
{
	if itemcount + i <= itemlimit && curnode.stock[itemID] >= i && money >= curnode.prices[itemID] * i
	{
		curnode.stock[itemID] -= i;
		money -= curnode.prices[itemID] * i
		inventory[itemID] += i;
		
		if i > 1
			var txt = "x "+string(i)+" -€"+string(curnode.prices[itemID] * i)
		else
			var txt = "-€"+string(curnode.prices[itemID] * i);
		floattext(mouse_x,mouse_y - 10,txt);
		for(var j = 0; j != i; j += 1)
		{
			if transferred[itemID] >= 0
				time += 1;
			transferred[itemID] += 1;
		}
		
		audio(a_coin,1 - min(0.05 * i, 0.5) + random(0.1),0.3)
	
		return(true);
	}
}

if (itemcount + 1 > itemlimit)
	floattext(mouse_x,mouse_y,"No inventory space!")
else if (curnode.stock[itemID] < 1)
	floattext(mouse_x,mouse_y,"No more stock!")
else if (money < curnode.prices[itemID] * 1)
	floattext(mouse_x,mouse_y,"Not enough cash!")

instance_create_depth(mouse_x,mouse_y,-100,o_nope);

return(false);