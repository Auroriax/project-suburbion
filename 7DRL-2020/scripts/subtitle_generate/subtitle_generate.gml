var str = ""

var prerand = random(1);
var sufrand = random(1);

var adjective = ["Utopian", "Underwater", "Neon-lit", "Brutalist", "Post-modern",
"Art Deco", "Graffiti-clad", "Underground", "Floating", "Bridge-connected", "Cyberpunk",
"Glass", "Uncanny", "Cable-lifted", "Collapsing", "Imposing", "Sinking", "Cutting-edge",
"Dystopian", "Historical", "Futuristic", "Pillar-carried", "Forgotten", "Fragile"]
var location = ["Canyon", "Mall", "Sanctuary", "Terminal", "Hill",
"Pool","Beach","Jungle","Discotheque","Factory", "Circuit", "Arcade", "Port", "Park",
"Distillery","Library","Station","Mine","Valley", "Statue", "Roundabout", "Labratory",
"Forest", "Mountain"]

str += array_random(adjective)
if prerand > 0.9
	str += ", "+ array_random(adjective)
else if prerand > 0.8
	str += " & "+ array_random(adjective)
	
str += " "+ array_random(location)
if sufrand > 0.9
	str += "-"+array_random(location)

return str;