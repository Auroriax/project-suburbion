{
    "id": "47b2c87d-2157-45ec-b51e-4ba7c3c8df78",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "f_bodytextbold",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": true,
    "charset": 0,
    "first": 0,
    "fontName": "Cooper Hewitt",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "84a7c847-c3f3-4bbf-91b6-eb378ba7bc2e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 21,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "aec6a7b6-7fe2-4075-81d8-9e8a7b310362",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 21,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 201,
                "y": 48
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "0dfd6f47-3812-4cdd-87bc-b0e3941661c6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 21,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 192,
                "y": 48
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "00c17c97-b77a-4f27-a3e2-dfca647443cf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 21,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 181,
                "y": 48
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "adde574a-47cf-4895-82c7-509288584ae1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 171,
                "y": 48
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "0a5a9882-9172-479f-b22e-cc312db05480",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 21,
                "offset": 1,
                "shift": 16,
                "w": 15,
                "x": 154,
                "y": 48
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "5801bb7e-6378-4add-9e25-f2f64002f5d3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 21,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 141,
                "y": 48
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "eb417ea6-65c2-4144-b807-75b68b87afb9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 21,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 135,
                "y": 48
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "9ed11caf-e0ac-440c-bc8e-22006c3d4a5c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 21,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 128,
                "y": 48
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "b17d23ee-0520-41aa-9c9c-d3a98d3bee28",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 21,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 120,
                "y": 48
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "745bafb8-11b0-43a3-a898-80fe7714a993",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 21,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 111,
                "y": 48
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "e873965b-4bf3-48df-b5a0-00c600d3794d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 101,
                "y": 48
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "dc7f40f8-61ce-4d78-a31c-319eccdc0a37",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 21,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 95,
                "y": 48
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "0d2f4b6b-55cf-4476-9350-94f2374dcb44",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 21,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 89,
                "y": 48
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "afbc2740-3585-4530-aa01-199dbd243644",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 21,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 84,
                "y": 48
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "17ab9044-991d-45d9-875c-25e4ace3035a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 21,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 75,
                "y": 48
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "e6fcc5f5-96d9-4195-9be2-9359d096b6fa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 65,
                "y": 48
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "895015f4-9467-486d-a1df-62a34ef56d9c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 21,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 54,
                "y": 48
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "bb0c6ebb-6d32-4944-8f29-eb279ed0dc7d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 44,
                "y": 48
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "ab65ddd6-8f91-4930-9786-903cd6f7a682",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 34,
                "y": 48
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "dfb9d3c7-20a0-4821-aed1-677a0871f891",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 21,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 23,
                "y": 48
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "54cb60e2-7011-4885-8658-b5dd98b58d8b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 13,
                "y": 48
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "8a9062ce-b8e1-43d1-bcd8-0aef0ae82f1c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 207,
                "y": 48
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "8afba7fb-cf2a-4288-bf10-e5e474c04156",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 218,
                "y": 48
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "93daba41-880f-493d-9084-c3864e1990c1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 229,
                "y": 48
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "e3530047-4828-46cd-ab7b-0a3df67898d8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 240,
                "y": 48
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "e37f8877-b4c9-4cd9-a775-4af0d4a7d384",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 21,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 229,
                "y": 71
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "4256e585-743b-46d2-9fa8-7141045c21de",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 21,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 223,
                "y": 71
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "ef2df0f8-e3df-4fa4-91f7-b8070d07beac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 212,
                "y": 71
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "e8d4730c-b7e1-48f9-b300-c8a916da97fb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 202,
                "y": 71
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "6ab68e2f-85cb-43ff-b044-df1369167286",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 191,
                "y": 71
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "161da5c2-7cb2-4b28-b371-f05106cfeef0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 21,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 181,
                "y": 71
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "502840d9-3490-4c38-ac03-3ab972b77dd4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 21,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 164,
                "y": 71
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "274bf389-572c-4d46-b386-6ab726668844",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 21,
                "offset": 0,
                "shift": 10,
                "w": 11,
                "x": 151,
                "y": 71
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "e01a0a12-7657-4e17-aa56-29338e5f6da7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 140,
                "y": 71
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "3198d989-4820-4ab5-9167-e6ab2b47178b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 21,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 129,
                "y": 71
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "aca954fd-2301-491f-b98e-fe9dd9cadc11",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 21,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 234,
                "y": 71
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "84dbab1a-1bf6-4df3-a256-ecf793807942",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 21,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 119,
                "y": 71
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "96c4c49a-9187-455a-a9bd-3032ae25cda8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 21,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 97,
                "y": 71
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "d998b36e-d07d-4a13-a83e-da4aa9244a98",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 21,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 85,
                "y": 71
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "2717dc00-8793-478a-8277-be778180ab95",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 21,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 74,
                "y": 71
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "08b852dc-6338-4394-83cc-647c54019b72",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 21,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 69,
                "y": 71
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "6988e02a-fd4f-45a2-800d-976f5ba36ea4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 21,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 59,
                "y": 71
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "cd541c3d-6477-4934-ad9a-b51d1f876078",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 48,
                "y": 71
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "09c00cb0-2faf-4f0c-ad1e-461b11efd3c7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 21,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 39,
                "y": 71
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "e6b06702-28f9-46d5-abf4-d50394165cc8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 21,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 25,
                "y": 71
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "b13182d3-bf68-454e-ac57-90cc9f106f47",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 21,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 14,
                "y": 71
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "14961a42-6df3-4cec-8bc3-9cfbd5700e8d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 21,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 2,
                "y": 71
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "c67286d5-0e4b-4415-b39c-9ef3d3fd6a3a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 2,
                "y": 48
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "b6f30633-87a9-4566-b404-2086849774ee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 21,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 107,
                "y": 71
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "5ea86c82-dd6f-4aaa-aa65-ee8eb749e89f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 237,
                "y": 25
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "17a8c249-17ea-499f-a581-689b1d1e729d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 21,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 85,
                "y": 25
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "cc247e6c-dc12-49a1-a097-ceb8c5acf466",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 210,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "c887c841-3454-4a68-b41d-b5a242e83d87",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 21,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 199,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "62e55c5f-7d52-4b5f-b894-ebf1503b345d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 21,
                "offset": 0,
                "shift": 10,
                "w": 11,
                "x": 186,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "ae2e9dbb-05d6-4b89-a89c-5af1c102dc1b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 21,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 169,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "55b9cc7d-c712-44f9-9a5e-b4806789fdb3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 21,
                "offset": 0,
                "shift": 10,
                "w": 11,
                "x": 156,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "159088a2-afaf-4275-927e-f2c7348ac47a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 21,
                "offset": 0,
                "shift": 10,
                "w": 11,
                "x": 143,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "a8f8ba2b-9e28-49f7-99df-3699631113be",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 133,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "bf28771f-46a3-4112-a2cc-a19509273d42",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 21,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 126,
                "y": 2
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "0166e6ba-32b4-4cba-8d37-135ad14fa548",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 21,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 117,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "737d5ad8-dd4b-4050-a8b7-afa6b3f05ca0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 21,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 110,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "07011543-09d2-47bf-b7f2-3d4c44f4be4c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 21,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 98,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "b825088a-0161-491d-a9ba-aebdb32125ce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 21,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 92,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "120cafa0-91e6-4ef8-8d44-de399f7826ef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 21,
                "offset": 1,
                "shift": 8,
                "w": 5,
                "x": 85,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "7765f77a-b2ae-4186-b0c6-71c767e2c44c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 21,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 75,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "a2178377-13fd-4a6b-aff0-a7551417b20f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 65,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "041b73cd-4804-4df2-a163-3d38abd5ad69",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 21,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 55,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "81883d24-b499-4b25-83ee-4a0c879b94e0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 45,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "a47c1db2-b11f-4225-a470-a65f31f0d3b6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 35,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "435bdc56-1f80-4136-996e-965125cab93c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 21,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 27,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "875d9c82-bbfe-42d8-ae91-969e44f7dc29",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 17,
                "y": 2
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "13b31a99-d6dd-4a31-ae93-d347a26b1ae2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 21,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 7,
                "y": 2
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "99f684f7-ff34-416c-a8a5-ff6017ee92d2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 21,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 220,
                "y": 2
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "83237ce9-ad58-45ea-990c-9dca84e3e429",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 21,
                "offset": -1,
                "shift": 4,
                "w": 5,
                "x": 225,
                "y": 2
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "2e283c92-461b-4060-8ed8-cbdf8e7df702",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 21,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 232,
                "y": 2
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "e5baddf2-1e98-4981-9706-2433fefeee29",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 21,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 242,
                "y": 2
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "d1da612d-adcd-43d6-9a10-30743f5e00a7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 21,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 192,
                "y": 25
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "3f7ac1af-606b-44d1-bbf3-05582aa1b16f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 21,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 182,
                "y": 25
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "31939eb1-0da4-4817-a4b6-97a59bbb676e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 172,
                "y": 25
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "23493c80-37f3-4b8b-bbd5-9f4fc57bc2aa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 162,
                "y": 25
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "f480e73b-f207-4141-a815-b73bfa784aa5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 152,
                "y": 25
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "4902c006-0550-4a78-a8b4-6a5e6980aa2b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 21,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 145,
                "y": 25
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "7bd799d7-8635-43a9-9102-679fa91e5537",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 21,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 136,
                "y": 25
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "92e761ad-06ec-470f-aa57-293aa6399ebb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 21,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 128,
                "y": 25
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "bef805b3-55b7-434b-b809-fb4245de856a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 21,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 118,
                "y": 25
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "aa436363-01d4-4f82-9df3-11fdd17c1d18",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 21,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 107,
                "y": 25
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "620ee8f7-380f-4540-8583-2caebccfde77",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 21,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 207,
                "y": 25
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "2250405c-9bbf-4449-8705-276eb70f0f95",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 21,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 96,
                "y": 25
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "3b1bd41b-3ae0-479f-9246-e688e4a88f6d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 21,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 74,
                "y": 25
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "c8b4110c-c809-4f81-98b6-57d7d28be6c0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 21,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 65,
                "y": 25
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "f2da6c67-756e-4af8-b468-4f5f227f10c6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 21,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 57,
                "y": 25
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "e977505f-22db-46ca-89ef-fd2731b9e45c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 21,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 52,
                "y": 25
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "82d53aa7-ecf9-4a53-a4e9-ec20dc1def48",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 21,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 44,
                "y": 25
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "666c5c5f-a742-4d4c-941f-4ee2159d69bf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 21,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 32,
                "y": 25
            }
        },
        {
            "Key": 215,
            "Value": {
                "id": "69126619-5cae-40cf-ad76-b8f6c7ea609b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 215,
                "h": 21,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 20,
                "y": 25
            }
        },
        {
            "Key": 8226,
            "Value": {
                "id": "857b9635-60d2-49a7-bac1-9ef603cdfa91",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8226,
                "h": 21,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 13,
                "y": 25
            }
        },
        {
            "Key": 8364,
            "Value": {
                "id": "93ffaf16-e07a-4470-9856-23efa3d4b9ea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8364,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 2,
                "y": 25
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "8e0ca3e7-71cb-4920-9b6f-7faf43c1bd98",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 21,
                "offset": 3,
                "shift": 10,
                "w": 4,
                "x": 247,
                "y": 2
            }
        },
        {
            "Key": 9650,
            "Value": {
                "id": "6e1a53ed-8437-42e8-a75e-5309edd03145",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9650,
                "h": 21,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 221,
                "y": 25
            }
        },
        {
            "Key": 9660,
            "Value": {
                "id": "43d31b1c-dec2-4683-ae59-615e557fcc36",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9660,
                "h": 21,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 2,
                "y": 94
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 215,
            "y": 215
        },
        {
            "x": 8226,
            "y": 8226
        },
        {
            "x": 8364,
            "y": 8364
        },
        {
            "x": 9647,
            "y": 9647
        },
        {
            "x": 9650,
            "y": 9650
        },
        {
            "x": 9660,
            "y": 9660
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 12,
    "styleName": "Bold",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}