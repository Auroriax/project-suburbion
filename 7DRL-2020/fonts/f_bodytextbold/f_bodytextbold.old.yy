{
    "id": "47b2c87d-2157-45ec-b51e-4ba7c3c8df78",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "f_bodytextbold",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": true,
    "charset": 0,
    "first": 0,
    "fontName": "Cooper Hewitt",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "7626248f-5505-4795-b44c-91ea1e873cc7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 21,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "2e927701-5712-465a-97f1-47ced17c42ff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 21,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 190,
                "y": 48
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "7d093446-3d1d-4e9d-b284-f6eeb88c329c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 21,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 181,
                "y": 48
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "9e51ba01-12ca-480a-8b16-0b0c54dbea6f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 21,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 170,
                "y": 48
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "84d9e1a2-1d4e-4406-a072-8537cfd43dff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 160,
                "y": 48
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "bc43107b-423d-4914-9551-ece12b31f979",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 21,
                "offset": 1,
                "shift": 16,
                "w": 15,
                "x": 143,
                "y": 48
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "e641c38d-e266-4367-9bf8-20cb01a9c7a2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 21,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 130,
                "y": 48
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "cd70e8ce-ffb2-458c-b41e-5988a879fcd9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 21,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 124,
                "y": 48
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "4916d844-2b9d-474d-88f1-4e769dc53175",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 21,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 117,
                "y": 48
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "cdad3fad-182b-41c7-a61a-07ad7c5bcd2d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 21,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 109,
                "y": 48
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "28575340-ae3c-4731-975e-f9d139597b8b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 21,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 100,
                "y": 48
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "53a70d30-9067-41fe-9dff-7f6202c5056c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 90,
                "y": 48
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "ee952c27-323c-4b87-812d-9624482f85be",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 21,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 84,
                "y": 48
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "4169de83-5e85-483d-98c0-a356a14ca21b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 21,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 78,
                "y": 48
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "b20f4403-2c9c-414c-a8fa-35e33e3e281b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 21,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 73,
                "y": 48
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "b357323c-b62e-44d1-b56b-f69814f43325",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 21,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 64,
                "y": 48
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "66ec6247-81c4-44b0-8581-5fa72dc467c8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 54,
                "y": 48
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "6a66e330-897f-49ac-a1be-6108215b3e8a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 21,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 43,
                "y": 48
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "9a249526-f6bf-4372-bda6-9797f834b1ef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 33,
                "y": 48
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "badf71d2-2355-4484-90e6-3d36375211cb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 23,
                "y": 48
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "4ef42083-e9e8-496d-b83d-b3a333271a94",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 21,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 12,
                "y": 48
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "a2672fae-f418-4d21-9fa7-8e3f367d6114",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 2,
                "y": 48
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "e4dd2b4f-963e-492d-a010-d21a12d8fa6d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 196,
                "y": 48
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "07100cca-01e5-43b0-b6f1-7bfdcb2f335c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 207,
                "y": 48
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "9d90ddff-e939-42b8-ab1f-9c0760fc82fa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 218,
                "y": 48
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "7ed62615-9980-4e1a-a686-4658a48fad72",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 229,
                "y": 48
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "960d9599-96ba-40c9-a96e-b979fcba9907",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 21,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 221,
                "y": 71
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "62272ce2-a846-419c-a6e6-e848800c442a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 21,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 215,
                "y": 71
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "876eb3c4-4bf3-412b-aa9b-4434b6421cd5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 204,
                "y": 71
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "d7820510-9711-461f-9099-cf5814605cec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 194,
                "y": 71
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "60cc9a14-7612-4f51-99ba-4a4ed878748e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 183,
                "y": 71
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "48635179-0416-49bd-b4d6-cc3e598cf397",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 21,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 173,
                "y": 71
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "704e8ebc-931d-4a6a-8b37-0b3a9c6db9d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 21,
                "offset": 1,
                "shift": 17,
                "w": 15,
                "x": 156,
                "y": 71
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "55bf92f5-9239-44bb-85ef-9c8a36d23831",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 21,
                "offset": 0,
                "shift": 10,
                "w": 11,
                "x": 143,
                "y": 71
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "a7e3be1c-4f72-4200-939b-f512c3bcb455",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 132,
                "y": 71
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "f8d5c46d-73ad-4c39-8982-de855a538c94",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 21,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 121,
                "y": 71
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "7e7f50d1-e1a5-4e2f-9db8-4485791c066e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 21,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 226,
                "y": 71
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "699893fe-492d-4807-872d-ef2b64e6ae14",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 21,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 111,
                "y": 71
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "54f46505-d155-4808-b78c-3874cbdfcab4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 21,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 85,
                "y": 71
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "4d95cf0f-ae0a-443d-a944-d8e75658cdc6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 21,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 73,
                "y": 71
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "91d8ba12-533f-46cb-989a-c0963824e663",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 21,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 62,
                "y": 71
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "7970cf82-ee1a-4f95-b4ff-17f8b8b1d3c7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 21,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 57,
                "y": 71
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "2a134f54-f5d9-480b-8e11-cba158670be1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 21,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 47,
                "y": 71
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "27fb2a95-d115-4f45-ab71-46194ed21392",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 36,
                "y": 71
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "f2be6e61-73c4-4eae-91ad-45bddcdc1b55",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 21,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 27,
                "y": 71
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "20e6b3d3-cea9-4869-8cd0-06ad40be2278",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 21,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 13,
                "y": 71
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "91783c95-6398-48df-ab00-31ad816589f9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 21,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 2,
                "y": 71
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "02ab42d2-b57a-455e-9d01-a90bbb1f962f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 21,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 240,
                "y": 48
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "f46538a1-3d1e-4373-b29c-500ad076f9d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 236,
                "y": 25
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "00d903cb-6b9d-413c-ae1c-237b70bb5e9b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 21,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 224,
                "y": 25
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "bf03133c-5009-4d5c-a212-4e7f57781913",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 213,
                "y": 25
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "9d044330-56a3-4b1b-b002-d17e33fbec36",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 21,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 225,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "e1b9a8be-8b1a-46a3-9c0a-09f059053070",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 210,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "7d24332b-619f-4014-af4a-06475a4d05a2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 21,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 199,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "cf340731-135e-4ebd-b5a7-f217a6d8a504",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 21,
                "offset": 0,
                "shift": 10,
                "w": 11,
                "x": 186,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "33cc2dc4-e4de-4d93-bb97-dfcab1ffafbb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 21,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 169,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "7f286ade-5d69-41af-a30d-863189dbbc56",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 21,
                "offset": 0,
                "shift": 10,
                "w": 11,
                "x": 156,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "c12f5c78-f88a-4b4e-970c-7789fdd37aa4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 21,
                "offset": 0,
                "shift": 10,
                "w": 11,
                "x": 143,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "6b9d02ab-d8e2-49b2-80b9-5b5fc862f86e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 133,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "f1e9c40b-1131-412a-a3fe-bbcc2a461073",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 21,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 126,
                "y": 2
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "82f4c7ce-2fd9-4a2e-a507-3c4075c5b81f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 21,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 117,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "3660423e-eec6-4649-9373-ef3ce28d312b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 21,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 110,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "19011285-1198-4ba8-b3bd-428aa03fc9b5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 21,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 98,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "e1dbcff0-ae25-4fb7-945f-e6e6cb052d7f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 21,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 92,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "e9f62075-b4c3-4c22-aa7e-db53df47fca0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 21,
                "offset": 1,
                "shift": 8,
                "w": 5,
                "x": 85,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "fd334d87-165a-488a-a3e2-4538ccfe1683",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 21,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 75,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "e4262024-9654-49c5-a6f9-e3ef24a37b5f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 65,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "baec8a5d-58a8-4972-bd85-b858e10ac741",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 21,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 55,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "f9a71795-5c0c-4ac5-a1be-499a1f4d4fd4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 45,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "f85e8bf0-782c-4f0e-b178-6279b57db3e0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 35,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "dbd5a8a9-e761-43a0-9d96-5163f30fb2b2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 21,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 27,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "061c2a7a-695f-4ec6-8316-2ae25190a4c1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 17,
                "y": 2
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "4a51c439-337b-4d2f-8e1d-dbf559ea7ccd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 21,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 7,
                "y": 2
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "7b633954-1529-45f2-9299-bf6108ae0702",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 21,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 220,
                "y": 2
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "04151af8-de24-485c-9d59-367638838fb1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 21,
                "offset": -1,
                "shift": 4,
                "w": 5,
                "x": 236,
                "y": 2
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "67d4cf28-438f-478d-8cf1-298dd077f3a8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 21,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 203,
                "y": 25
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "c5299997-3a2d-4f5a-a75d-38799172bd24",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 21,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 243,
                "y": 2
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "c0e10dad-2a49-46a8-b0fc-72500b3e81cb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 21,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 188,
                "y": 25
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "f700581f-e70a-4e0f-8795-bab5b5ce5bcd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 21,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 178,
                "y": 25
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "325076ad-cac5-4d99-b2ab-520ccd89f8c4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 168,
                "y": 25
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "3e970579-95f7-40af-b2be-e04d2c831bfb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 158,
                "y": 25
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "07c8b333-dab5-4e59-bc70-a40d88930e3a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 148,
                "y": 25
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "22b4ada8-7f55-427b-bdd3-c0fa5af882d5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 21,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 141,
                "y": 25
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "5e0f0832-118d-4b65-84b9-6406d2a9e2df",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 21,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 132,
                "y": 25
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "9ef3975b-8dad-4ac8-a067-aeb40e8ec99b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 21,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 124,
                "y": 25
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "6d902896-c624-4c70-bf77-2c94babe35ac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 21,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 114,
                "y": 25
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "a0f5a00e-8535-4704-b74d-9bff4e0d09c6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 21,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 103,
                "y": 25
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "bfab74f0-df8d-4966-9876-27b0b1b36d6b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 21,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 89,
                "y": 25
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "9ef2f1b7-9f84-46aa-af86-b9baefc56dc0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 21,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 78,
                "y": 25
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "f56f8c35-dcba-437f-9d18-a7483461a41e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 21,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 67,
                "y": 25
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "12fd39d6-79ae-4fd0-afc2-bbad50310fd6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 21,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 58,
                "y": 25
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "d20d3648-840a-4530-b520-ad79d11e863c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 21,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 50,
                "y": 25
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "fe9472ba-6bf0-45a7-a873-dd22300f96db",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 21,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 45,
                "y": 25
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "d7af1635-3190-44a1-a12b-eadc0706a703",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 21,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 37,
                "y": 25
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "874ce593-8633-423a-b7b9-23da6f92207c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 21,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 25,
                "y": 25
            }
        },
        {
            "Key": 215,
            "Value": {
                "id": "d74429ac-72bd-4f9e-8a60-5f987bbd1286",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 215,
                "h": 21,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 13,
                "y": 25
            }
        },
        {
            "Key": 8364,
            "Value": {
                "id": "f6ee33fb-95e0-4467-a57f-d6c3a9b851a6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8364,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 2,
                "y": 25
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "ac15d588-5202-4b56-920f-ac049ed775e6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 21,
                "offset": 3,
                "shift": 10,
                "w": 4,
                "x": 248,
                "y": 2
            }
        },
        {
            "Key": 9650,
            "Value": {
                "id": "1ac464fe-1c07-49c4-9eed-3907e05f8397",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9650,
                "h": 21,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 95,
                "y": 71
            }
        },
        {
            "Key": 9660,
            "Value": {
                "id": "4f930b93-58b4-468c-8165-e24efea96bb3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9660,
                "h": 21,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 237,
                "y": 71
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 215,
            "y": 215
        },
        {
            "x": 8364,
            "y": 8364
        },
        {
            "x": 9647,
            "y": 9647
        },
        {
            "x": 9650,
            "y": 9650
        },
        {
            "x": 9660,
            "y": 9660
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 12,
    "styleName": "Bold",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}