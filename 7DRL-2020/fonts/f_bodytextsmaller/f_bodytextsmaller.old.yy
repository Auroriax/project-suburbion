{
    "id": "ca5aa05d-5660-4c60-8e9f-b7e1ea076c60",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "f_bodytextsmaller",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": true,
    "charset": 0,
    "first": 0,
    "fontName": "Cooper Hewitt",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "ec5d34b9-83e0-4231-8877-1bff7f2f951c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 17,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "b28d224b-95da-447f-8455-3a53863c7728",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 17,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 116,
                "y": 40
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "c5426dfe-d3a0-4062-a076-c0eb52c275e5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 17,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 109,
                "y": 40
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "f0ead7a4-b3a0-4598-b142-a3c85370e93b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 17,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 100,
                "y": 40
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "e0b80941-d681-4aad-bed8-dc49fc237523",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 17,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 91,
                "y": 40
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "37c07731-91ef-4df7-8ac2-47b6906b16ce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 17,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 75,
                "y": 40
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "5775ea7a-2fba-439f-8fe5-6fc08337a5af",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 17,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 64,
                "y": 40
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "9e6a536d-69a1-447b-89eb-9606e756a27f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 17,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 59,
                "y": 40
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "b99f6454-e02c-47c3-aa34-325001eec3c3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 17,
                "offset": 1,
                "shift": 5,
                "w": 5,
                "x": 52,
                "y": 40
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "af0a2485-e22d-48a6-b879-a0d0e72a5123",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 17,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 45,
                "y": 40
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "9d7ce279-7303-489b-aa3a-80d4e293d484",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 17,
                "offset": -1,
                "shift": 5,
                "w": 7,
                "x": 36,
                "y": 40
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "74c01e47-63ec-4c70-acc9-c5fa44873f92",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 17,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 27,
                "y": 40
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "8db47909-07ee-417c-a325-c15780d35296",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 17,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 21,
                "y": 40
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "4e59d32a-969a-4931-94c1-b49de541a3f2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 17,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 15,
                "y": 40
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "5898590f-9408-4a6c-b2cf-a293d534af9f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 17,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 10,
                "y": 40
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "dce5f67f-3b00-4f02-be45-b77fd534e9ee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 17,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 2,
                "y": 40
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "b2fba51f-f48a-4fa2-8e4b-e40e5f83bf29",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 17,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 245,
                "y": 21
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "5f9c2990-eaa5-4e59-8b1d-5ae807623db0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 17,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 235,
                "y": 21
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "d88d8f74-7057-47c4-9147-8ab119cd4df6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 17,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 226,
                "y": 21
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "1e625d12-e9cf-47d3-af15-98eab77781b9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 17,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 217,
                "y": 21
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "c87687cd-7f3a-47fd-93b1-15840ce448aa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 17,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 208,
                "y": 21
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "1692394e-e99a-403c-ae3e-c9c884a30640",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 17,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 199,
                "y": 21
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "207670ec-1431-43a5-a0fc-ef3a0d1db6dc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 17,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 122,
                "y": 40
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "a8bf22c8-81b1-4b6b-917e-9dfde8a52af3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 17,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 131,
                "y": 40
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "6555ea0d-f2d3-4293-b021-dac50c42e1ca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 17,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 140,
                "y": 40
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "e9a59b45-7d82-4ee4-8c3b-a7d0b07cecce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 17,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 149,
                "y": 40
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "e3239905-5320-4884-9a7c-c6c4690b000a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 17,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 112,
                "y": 59
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "d6ab2586-67d1-45b2-8c07-777b64eeec3c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 17,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 106,
                "y": 59
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "5bb11e9f-f723-4dee-84ee-679ddddd5a40",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 17,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 97,
                "y": 59
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "a1b8459a-bafd-4067-b2a6-87a0ad82bf07",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 17,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 88,
                "y": 59
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "9f1c492e-a066-4f1e-a180-bad32f813681",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 17,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 79,
                "y": 59
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "ddc45ac4-42e0-493c-bb49-48b8618f8946",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 17,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 70,
                "y": 59
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "8bbb213e-f7fe-4c6c-a5e6-84d203b04bb2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 17,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 55,
                "y": 59
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "cd9268b2-2df9-4a12-9372-e342173c3f59",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 17,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 44,
                "y": 59
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "58257a93-69aa-4f7b-9d37-bf3b5d374353",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 17,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 35,
                "y": 59
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "5549ac6a-55c2-4e55-a18e-89983a03d83c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 17,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 25,
                "y": 59
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "cddb6536-63dc-4cae-8860-7d7c66cd807c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 17,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 117,
                "y": 59
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "7fc0cd22-b23b-4337-9eb0-efa910aac7d5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 17,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 16,
                "y": 59
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "111b8b69-0086-4ed4-b309-18621ceda538",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 17,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 243,
                "y": 40
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "fd647662-001b-4121-a425-ffb948431605",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 17,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 233,
                "y": 40
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "f0c31f15-6b3a-4c1a-8131-f6bfc3c61b73",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 17,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 223,
                "y": 40
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "3437d3a0-0dee-4299-bddd-261e672dd897",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 17,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 218,
                "y": 40
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "6807c418-b25d-451b-b9c0-5ce0b0eb06d7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 17,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 209,
                "y": 40
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "c987628a-9cf3-45e8-9259-2010bfdd6ee5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 17,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 199,
                "y": 40
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "e2ee649c-5ea5-4d79-ab0b-a3cc1845ca0d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 17,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 191,
                "y": 40
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "2958b381-7842-47ad-8c39-1ff6c04e21ea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 17,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 178,
                "y": 40
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "04ef1d91-e979-4332-80e5-cf97fec09d37",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 17,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 168,
                "y": 40
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "ccbe529e-4986-4e03-a035-e03c820fc696",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 17,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 158,
                "y": 40
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "1097d658-608e-4fbb-93f4-cbcf0b27b2bd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 17,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 190,
                "y": 21
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "e0a583be-798b-4d07-9641-92231f7c5543",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 17,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 180,
                "y": 21
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "161e9079-8c84-4669-8514-f226d937b2bb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 17,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 171,
                "y": 21
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "46ebb8b2-6e3b-478e-92a7-9f41ba62c4c9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 17,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 201,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "45f2bad3-6a3b-4dcb-814e-311df6da3c0a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 17,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 187,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "dded1873-d503-441f-87a1-c9a51eaffed9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 17,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 177,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "70081865-17ac-4ff5-ad54-e646bf5fc752",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 17,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 166,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "59c5d621-dc5a-4122-ab1e-a1a16e372a8a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 17,
                "offset": 0,
                "shift": 12,
                "w": 13,
                "x": 151,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "4eed72a5-74a1-4ec5-8918-973ef5fa4fb0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 17,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 140,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "5ea24d58-d1ba-4f45-95af-014e224f6dd7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 17,
                "offset": 0,
                "shift": 8,
                "w": 9,
                "x": 129,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "ff536805-b239-4dc1-ade4-0e48f0f8a583",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 17,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 120,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "751cb5a2-570a-409f-a6d7-1af60e49995e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 17,
                "offset": 1,
                "shift": 5,
                "w": 4,
                "x": 114,
                "y": 2
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "331c6c37-3500-4e52-9a0d-38141c42b810",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 17,
                "offset": 0,
                "shift": 5,
                "w": 6,
                "x": 106,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "6a2c45e7-4409-49d0-a52b-f3240367096c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 17,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 99,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "6caef0c7-f1c3-4198-8abf-70f52d526698",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 17,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 89,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "65b9f7c2-da44-4709-8d6a-58f03464e97f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 17,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 84,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "9056acd8-a9f6-4a25-93de-a8ace618d90f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 17,
                "offset": 0,
                "shift": 7,
                "w": 5,
                "x": 77,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "852baa8a-771b-4f13-bdb7-f154db7d65c3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 17,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 68,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "62560677-2133-4b89-9595-3078f666a3ad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 17,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 59,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "aec26cdd-f9ff-4f18-bf7b-d9b99e534320",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 17,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 50,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "f2e2a599-9065-4e71-b4cd-50120d1244c6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 17,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 41,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "2670d5af-025a-45cf-b71f-bd56c02f4b90",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 17,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 32,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "a7fbeb5a-df8d-4c1e-91ed-e7fc9f0a1ca4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 17,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 25,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "eea89a88-8ffa-4ae4-a9e6-64506e74ad19",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 17,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 16,
                "y": 2
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "3cc44f80-eca9-4296-9e4d-ac519ac124d8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 17,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 7,
                "y": 2
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "05aa2fc3-693b-45cd-9141-dc48bc40b8b8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 17,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 196,
                "y": 2
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "e8d53347-ccb8-42d9-b840-3ab10196c6b4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 17,
                "offset": -1,
                "shift": 4,
                "w": 5,
                "x": 211,
                "y": 2
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "f49a6bed-c861-45b6-849e-5db4cb4913d3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 17,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 162,
                "y": 21
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "586ba7d5-bea2-45db-891a-cef1d32727cf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 17,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 218,
                "y": 2
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "60fba4d4-b71c-4e7d-b3ee-817d3ff7252a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 17,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 149,
                "y": 21
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "b7d3270c-af6c-48d8-bee4-c2cf9256f23c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 17,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 140,
                "y": 21
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "3a1faede-3fe4-49c8-b5ed-2595d33487d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 17,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 131,
                "y": 21
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "baae775a-2b22-405d-9820-8a288fa780a3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 17,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 122,
                "y": 21
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "e670c464-6b95-4bb9-b783-a0e7d5716429",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 17,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 113,
                "y": 21
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "0d2267bd-ebdb-4998-82de-b3bdbcb1e78c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 17,
                "offset": 1,
                "shift": 6,
                "w": 4,
                "x": 107,
                "y": 21
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "51cf7a4a-ace6-4616-b7e4-d70f2f29f75a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 17,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 98,
                "y": 21
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "c54e67d6-b97a-465c-8fa0-0a14d91cc044",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 17,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 91,
                "y": 21
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "0e3296c6-19f3-4ce4-97ab-6c5c5d1f455e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 17,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 82,
                "y": 21
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "1b949493-8237-4ec6-b85d-727fd8fa8264",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 17,
                "offset": 0,
                "shift": 7,
                "w": 8,
                "x": 72,
                "y": 21
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "3f19437d-413b-4c22-bd7e-ee6b6a83ca55",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 17,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 60,
                "y": 21
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "ad108650-1504-4313-9a00-4acb70ea5979",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 17,
                "offset": 0,
                "shift": 7,
                "w": 8,
                "x": 50,
                "y": 21
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "17aebc70-d755-4a2e-9633-2f186ed65124",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 17,
                "offset": 0,
                "shift": 7,
                "w": 8,
                "x": 40,
                "y": 21
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "1a5608a4-d80b-47eb-a1de-039244562773",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 17,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 32,
                "y": 21
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "250406d3-1d9b-4cd5-9236-ac838eedeb90",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 17,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 25,
                "y": 21
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "94aa04d4-3059-4179-ab90-a3a312d698a7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 17,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 20,
                "y": 21
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "01b803b7-a1df-42e0-b728-e2caf81b9779",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 17,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 12,
                "y": 21
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "5203929f-5b29-4c87-bcae-4aecec1d9935",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 17,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 2,
                "y": 21
            }
        },
        {
            "Key": 215,
            "Value": {
                "id": "c426fcdb-58e6-4c01-9e36-17a3079ee222",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 215,
                "h": 17,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 239,
                "y": 2
            }
        },
        {
            "Key": 8364,
            "Value": {
                "id": "d32803ae-c099-476d-b224-2022f1896768",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8364,
                "h": 17,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 229,
                "y": 2
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "ea064f8f-d896-4fcf-a574-da4c5a990dd7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 17,
                "offset": 2,
                "shift": 8,
                "w": 4,
                "x": 223,
                "y": 2
            }
        },
        {
            "Key": 9650,
            "Value": {
                "id": "00dce9fa-b91e-429a-ae57-6f0b1ed642bb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9650,
                "h": 17,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 2,
                "y": 59
            }
        },
        {
            "Key": 9660,
            "Value": {
                "id": "5b30c862-aee8-4fc5-b17d-1286dd7f4757",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9660,
                "h": 17,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 127,
                "y": 59
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 215,
            "y": 215
        },
        {
            "x": 8364,
            "y": 8364
        },
        {
            "x": 9647,
            "y": 9647
        },
        {
            "x": 9650,
            "y": 9650
        },
        {
            "x": 9660,
            "y": 9660
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 10,
    "styleName": "Bold",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}