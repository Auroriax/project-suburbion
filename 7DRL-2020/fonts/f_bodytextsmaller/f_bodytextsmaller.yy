{
    "id": "ca5aa05d-5660-4c60-8e9f-b7e1ea076c60",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "f_bodytextsmaller",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Cooper Hewitt",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "2456de16-6a6e-4f1d-be66-d2c34c2b028c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 17,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "2c638349-63e1-4661-a27d-51ba3e25a862",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 17,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 105,
                "y": 40
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "40d00470-367c-45a9-9b97-f08dfce6d070",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 17,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 98,
                "y": 40
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "8bf5821a-d41f-4017-ad70-eeb961d46619",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 17,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 88,
                "y": 40
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "231fb48b-c76d-480b-9f65-e836bc0816fb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 17,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 78,
                "y": 40
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "61557bd5-22f0-4c81-a579-3b6104c6255b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 17,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 64,
                "y": 40
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "db0980fb-dd38-4d4b-8c3a-07f4988ca870",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 17,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 53,
                "y": 40
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "4d8c8d08-159c-4c62-bad8-e77d5394d63f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 17,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 48,
                "y": 40
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "dd5ad9f0-3573-441d-b612-2a703ca59316",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 17,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 42,
                "y": 40
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "fe404981-74f8-4c16-93be-1b7ebd4ea465",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 17,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 36,
                "y": 40
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "4cf25568-9c58-4753-867c-a1b531230ea5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 17,
                "offset": 0,
                "shift": 5,
                "w": 6,
                "x": 28,
                "y": 40
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "e2381417-060b-4c87-834e-ad4be66569b8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 17,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 18,
                "y": 40
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "3f9edc85-c0a8-4d1c-a3d4-c34c2690db94",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 17,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 13,
                "y": 40
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "39a6dff8-e612-4367-aad1-8b031551da6c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 17,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 7,
                "y": 40
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "2676dfdf-7614-47b2-9481-c7af4e51a830",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 17,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 2,
                "y": 40
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "df5a016b-a729-418f-82b0-4d43aae62958",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 17,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 246,
                "y": 21
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "e0196a1f-ab1f-416b-ba9b-9ed636784aaa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 17,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 236,
                "y": 21
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "5960d8ad-f29e-4e1f-8b07-d59943c71471",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 17,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 226,
                "y": 21
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "44c06d95-0af8-4632-aa98-753bd50ff27e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 17,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 216,
                "y": 21
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "2d712c95-4fea-4507-9a31-7510f4a5c53e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 17,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 206,
                "y": 21
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "5f20da50-b25b-4507-94c2-32434302c696",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 17,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 196,
                "y": 21
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "f0419434-712e-40bc-9be5-d3f9ce0df13f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 17,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 186,
                "y": 21
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "f4a923aa-ffdd-4c8f-8fc4-2745a90b19cb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 17,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 110,
                "y": 40
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "90ab9fc7-71eb-4d13-ab33-25e2860981aa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 17,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 120,
                "y": 40
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "0ea3221a-8ec0-4f0b-a794-4eb49325c620",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 17,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 130,
                "y": 40
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "756f0f4c-76e9-48a3-8c6b-7dd6106c5e13",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 17,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 140,
                "y": 40
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "a50a2694-c2ac-4368-9c60-5f6d0fbaee63",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 17,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 113,
                "y": 59
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "dacf2f4f-898a-4960-be81-a80565e66a6b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 17,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 108,
                "y": 59
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "3570139a-9e75-4b03-9e67-e8b8adeec2cc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 17,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 98,
                "y": 59
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "20f4ed4b-e59f-4f37-85da-b258d6de3204",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 17,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 88,
                "y": 59
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "fb9652af-a3da-47d1-9bb3-3abef358d790",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 17,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 78,
                "y": 59
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "ac30c081-e413-46c2-b5e1-4b03f5d6e63f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 17,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 70,
                "y": 59
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "07f3cdfe-1acd-4ece-ab7a-9c89d1a3f66b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 17,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 55,
                "y": 59
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "d71a9f94-209d-4dfa-b188-d61eedbb0ade",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 17,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 45,
                "y": 59
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "e7551de9-dd4a-41be-95d6-5521af281a9f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 17,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 35,
                "y": 59
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "116c9b04-ae70-47c9-ba97-c6985696f98a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 17,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 25,
                "y": 59
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "270c1952-858e-47f4-ab18-3181259fab44",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 17,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 118,
                "y": 59
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "60b13ba9-4368-40f8-8efe-c9c3dd12a253",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 17,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 16,
                "y": 59
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "cd41dfd3-78fe-4009-9481-c7c1c4773386",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 17,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 234,
                "y": 40
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "ce175d1c-ed92-47d7-9e26-e80df2f128fc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 17,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 224,
                "y": 40
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "9c87ed7e-4e82-4d93-b3a6-50269f4037d1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 17,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 214,
                "y": 40
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "6de686dc-bedb-4e04-9d68-1a48d6b59994",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 17,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 209,
                "y": 40
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "5a5a4205-08b6-4e40-a300-42070263639b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 17,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 201,
                "y": 40
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "34529ca6-b29e-4b1b-aa42-25c32bc2f26b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 17,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 191,
                "y": 40
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "16aeb567-389f-4998-9de9-c51093151171",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 17,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 182,
                "y": 40
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "49e82ae3-3daf-45a8-adfc-ba30e47d20d5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 17,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 170,
                "y": 40
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "123f1730-a00c-4d40-809f-f8839ee69789",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 17,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 160,
                "y": 40
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "fdb259ac-0631-463a-8e75-5a99c063dfac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 17,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 150,
                "y": 40
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "aaac6127-be5e-496f-bc99-c812f089b9d0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 17,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 176,
                "y": 21
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "54406180-de52-433d-917e-bdc0b656b5e8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 17,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 166,
                "y": 21
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "f7ecbbff-b492-47d7-ac0a-d30f877eedc3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 17,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 156,
                "y": 21
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "d3c5915f-30b2-408d-b54f-3fd089c063be",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 17,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 194,
                "y": 2
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "575dbd78-15f7-465b-be1d-0fc901a87903",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 17,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 180,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "7604b4f2-cb94-4aeb-a132-f452522e0e21",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 17,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 170,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "7e803d4b-9594-4007-bd09-3f01e1fa58b3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 17,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 160,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "18b3bbc8-e11c-4ae2-9133-20dd06c0dfc7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 17,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 146,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "57f44db0-62c4-47a9-91f5-1e39da3b3544",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 17,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 136,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "9d27bc10-f3bb-4af8-a872-1223676c6038",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 17,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 126,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "483b32a5-02e9-44e4-ba0d-2772095d17e9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 17,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 117,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "ecdd4d82-9564-4556-bf0f-344879e3cb05",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 17,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 111,
                "y": 2
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "439f606f-7be9-454a-a587-3084f379a91e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 17,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 104,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "8ab0386b-161f-4c48-bf5c-35be3d31434e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 17,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 98,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "9df22332-8333-4de7-a08b-18a6a07799db",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 17,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 88,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "67415641-4919-4989-a03a-095f71d0e396",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 17,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 82,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "f81289bb-773c-4b61-865c-eb676ec3d415",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 17,
                "offset": 1,
                "shift": 7,
                "w": 3,
                "x": 77,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "522d1ede-e9a1-4113-84d3-8ba7954d5448",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 17,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 68,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "1f97c2f7-5f25-49d5-bb41-8f82d814c85e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 17,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 59,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "069e1337-bb7b-4eaf-95fc-630caac04ade",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 17,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 50,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "c76843fb-4e1e-4b26-8344-e33408898bcf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 17,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 41,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "40cd5852-6e1a-4b35-9a61-91ee13de5823",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 17,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 32,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "47fe238b-590d-4f02-9cb4-2f45fa07df6e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 17,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 25,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "340373d2-c5ff-42c7-8746-5dfe7c552f4a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 17,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 16,
                "y": 2
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "5c79ba30-0859-46cc-ac53-9a752e9ab056",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 17,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 7,
                "y": 2
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "a061368b-f653-4c5d-8884-4e70ac42a43f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 17,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 189,
                "y": 2
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "27735f42-cc7f-4c3b-9204-e82109d5bcbe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 17,
                "offset": -2,
                "shift": 3,
                "w": 5,
                "x": 204,
                "y": 2
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "eec8ae5c-a33d-4ad7-911a-939782143c95",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 17,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 147,
                "y": 21
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "506c6ad2-4e4b-49fb-8031-46db6d6aacc6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 17,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 211,
                "y": 2
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "a99a9806-cc4d-4ad2-83df-ea33dc9d9b19",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 17,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 135,
                "y": 21
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "5f852f4b-5b98-4b83-b769-2aa710a91812",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 17,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 126,
                "y": 21
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "7fa33b01-96e7-4242-b7cb-7c94bc4b39a2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 17,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 117,
                "y": 21
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "53d1eda4-a592-46de-9390-e3e6aec75e23",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 17,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 108,
                "y": 21
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "6f5a5a33-2eb1-4023-8731-83d0900f9ab3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 17,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 99,
                "y": 21
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "7c81dccc-c2d9-43ab-b847-d9d735b37192",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 17,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 92,
                "y": 21
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "a0de4a6d-793f-4a69-9f74-466e258c3f75",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 17,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 83,
                "y": 21
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "4b7d78a7-696f-431b-8dfe-ca126c79726c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 17,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 76,
                "y": 21
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "28d7fb70-cac8-42fb-b56a-b8a9e4a03d04",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 17,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 67,
                "y": 21
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "3c2691cf-af7b-4b95-9320-55b786cdc5c1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 17,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 58,
                "y": 21
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "87802268-181c-48ad-aa8a-9e2cb7bfe379",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 17,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 46,
                "y": 21
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "16347dd2-3175-4e3a-a1af-a631329954f0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 17,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 37,
                "y": 21
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "20a284d9-840c-464e-92bd-4167fa97a264",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 17,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 28,
                "y": 21
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "c63a056b-9fc4-482e-8aa2-443d0246c3ed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 17,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 20,
                "y": 21
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "0c43c907-b5c3-41de-94d8-210cd6af700e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 17,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 14,
                "y": 21
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "12ededb8-9072-48fb-8b4c-ee81c997dd09",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 17,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 9,
                "y": 21
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "b6c977c4-59d7-461a-b3dc-bdaadf848d9e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 17,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 2,
                "y": 21
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "c9d9a8d6-81df-4dba-b90e-a012837af383",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 17,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 242,
                "y": 2
            }
        },
        {
            "Key": 215,
            "Value": {
                "id": "a1637fa4-f8df-46e1-8175-4bc3c93b3db8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 215,
                "h": 17,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 232,
                "y": 2
            }
        },
        {
            "Key": 8364,
            "Value": {
                "id": "7b205158-22b9-4714-8453-61956a7c8073",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8364,
                "h": 17,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 222,
                "y": 2
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "388c6a7e-3001-4f0f-bb44-59e75d999d69",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 17,
                "offset": 2,
                "shift": 8,
                "w": 4,
                "x": 216,
                "y": 2
            }
        },
        {
            "Key": 9650,
            "Value": {
                "id": "c9b9361a-3a0c-42ad-a11f-8b3afa45d7a6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9650,
                "h": 17,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 2,
                "y": 59
            }
        },
        {
            "Key": 9660,
            "Value": {
                "id": "4e046655-c49c-4d3a-b17c-128c38f64b25",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9660,
                "h": 17,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 128,
                "y": 59
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 215,
            "y": 215
        },
        {
            "x": 8364,
            "y": 8364
        },
        {
            "x": 9647,
            "y": 9647
        },
        {
            "x": 9650,
            "y": 9650
        },
        {
            "x": 9660,
            "y": 9660
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 10,
    "styleName": "Medium",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}