{
    "id": "44840b42-92cd-4839-9111-0aa4ba2f0d73",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "f_bodytext",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Cooper Hewitt",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "9426eec2-d226-4369-a06c-67060b361cf3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 21,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "d769ce6a-f822-4106-b770-5dec5ee5f711",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 21,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 217,
                "y": 48
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "e1e9c995-20fe-4d9a-8fae-9ed018b76630",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 21,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 209,
                "y": 48
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "d4361962-7e1a-42bd-836b-2f9541dabbea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 21,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 197,
                "y": 48
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "f2a2cd20-1ea4-4bcf-827b-715e9c0b5d2b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 21,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 186,
                "y": 48
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "53c28d8d-1716-4b93-8ce0-34bc9d9c0b8d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 21,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 169,
                "y": 48
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "76561dca-7309-4bb7-bd63-be5f8315b308",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 21,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 156,
                "y": 48
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "1dddc2f2-3a5d-4078-be09-e054dfc59bb6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 21,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 151,
                "y": 48
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "b20297cc-3831-497a-8f7d-d1ab365a6e0c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 21,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 144,
                "y": 48
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "d9be6c4b-067f-43d6-9475-35a03b368b5d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 21,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 137,
                "y": 48
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "204bee4e-6041-41ca-b4ba-31bc9386363b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 21,
                "offset": 0,
                "shift": 6,
                "w": 7,
                "x": 128,
                "y": 48
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "b9239c46-e816-4858-a728-92e1e880c067",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 21,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 222,
                "y": 48
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "3c4f1cbf-fd33-42fe-b058-39dfe77afdba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 21,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 123,
                "y": 48
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "014ffb2a-d6eb-42f9-b2e3-9af9cb593639",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 21,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 105,
                "y": 48
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "0c4e0ec2-b9d4-4ec3-a4a9-8bd052d3cc0f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 21,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 100,
                "y": 48
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "1d48e7b7-f349-4850-996f-80b258d58686",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 21,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 92,
                "y": 48
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "dd7819bd-c9b2-4d96-b9c6-b753153ac8f6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 21,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 81,
                "y": 48
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "5ae07994-a645-403b-bd4d-04b378a7f258",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 21,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 70,
                "y": 48
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "9db73521-4775-4d0b-b449-0ffb7b911110",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 21,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 59,
                "y": 48
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "c692c575-c949-4e80-ae75-8e79c97fb7f0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 21,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 48,
                "y": 48
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "79192e9a-df04-43d1-bbb4-5af53ee6a95a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 21,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 36,
                "y": 48
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "b2568a2c-0c92-45af-a753-5931edf18586",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 21,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 25,
                "y": 48
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "4f8e52f7-a2c4-49a2-816f-0d8e75cc6cc0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 21,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 14,
                "y": 48
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "29babbb2-4beb-40b9-b374-9ce98ea89c8b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 21,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 112,
                "y": 48
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "d8e3ee36-1c60-4135-b249-3ce1cfd425c7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 21,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 2,
                "y": 71
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "1534ce71-6458-4fbf-9ef1-41ff97e5adb0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 21,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 121,
                "y": 71
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "d5f06d15-c9a9-4b52-981b-1eeec518244d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 21,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 13,
                "y": 71
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "e1c3e955-83cf-4255-ad34-28add25fa61b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 21,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 248,
                "y": 71
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "becd7019-78f9-478f-b198-e247a8105734",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 21,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 237,
                "y": 71
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "7fb82583-3b0b-474e-9112-fa3cc8bbf157",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 21,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 226,
                "y": 71
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "5ecfe243-7914-491a-b21d-4289013c987e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 21,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 215,
                "y": 71
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "a8b16044-ce8f-443a-86cb-66a6d0120c6e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 21,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 205,
                "y": 71
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "2c5f43d9-0db7-4132-a85a-0c57984b05b0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 21,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 187,
                "y": 71
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "618c5b17-c16c-4f5b-98cf-77445937368a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 21,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 175,
                "y": 71
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "112a93e8-d539-45eb-b507-3a87c3eb8ada",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 165,
                "y": 71
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "40198e57-9cb5-4555-ac64-7ed585305d69",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 21,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 153,
                "y": 71
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "b7c17040-02e4-4d7c-84b1-88af2a9bc38b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 142,
                "y": 71
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "6c0e2332-98d3-4d9b-98e2-5306af816535",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 21,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 2,
                "y": 94
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "f431435e-b51f-4092-ba64-5ab0590940bb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 21,
                "offset": 1,
                "shift": 8,
                "w": 8,
                "x": 132,
                "y": 71
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "c4f64bc9-ccd3-4a95-9fbe-21d691494eae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 21,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 109,
                "y": 71
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "2aec94c8-32c8-4a2f-aed1-5bee3c9c785a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 98,
                "y": 71
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "fafab6ef-8f7b-4a09-819b-f0bbaf395979",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 21,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 94,
                "y": 71
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "173c0312-b7a7-48b2-9b8d-9a3df98dff35",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 21,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 84,
                "y": 71
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "82514ae3-8814-484f-96df-f3036b81fc05",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 21,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 74,
                "y": 71
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "735537d8-0f20-4d28-899b-37039613758a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 21,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 65,
                "y": 71
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "98bdd789-0c30-48d8-aa7f-a8a213e91bd1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 21,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 51,
                "y": 71
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "8ee4fb21-10c0-4b75-b43b-55d8d814aa4d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 21,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 40,
                "y": 71
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "bb1b412f-4cbc-44de-83a4-bfc421022456",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 21,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 28,
                "y": 71
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "b73b908f-2563-44bd-8c53-73141dffd96b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 21,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 18,
                "y": 71
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "be7154a5-ba15-4c29-92ba-ca226c15d3d3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 21,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 2,
                "y": 48
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "943ca358-0833-4b11-a8bd-c2ce3ffb85a0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 235,
                "y": 25
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "2768b0b2-1eb0-4de8-ac97-3a7bf753efbc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 21,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 223,
                "y": 25
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "b6d7fee8-d7a6-4d79-a92b-c0dcdd8ede01",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 21,
                "offset": 0,
                "shift": 8,
                "w": 9,
                "x": 89,
                "y": 25
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "e4847480-8187-4ed6-a45d-fa5b005fdf64",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 21,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 202,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "ab8fc963-2cb1-4884-9a18-19652f4f62f0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 21,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 190,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "fc031f7b-54c3-45b3-a1fc-42900ea43a1f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 21,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 173,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "e8ef4192-c295-491b-b2c7-de7d366b762c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 21,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 161,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "6c0a83dd-b2e0-40c1-a080-5bfd493d4378",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 21,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 149,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "c125f6db-30d8-4859-92cd-21b886a8cc60",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 21,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 138,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "bb4aaf7d-0167-47d7-9b1d-b5ed0fba1668",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 21,
                "offset": 1,
                "shift": 5,
                "w": 4,
                "x": 132,
                "y": 2
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "f09f6c9c-2f00-4481-b2a3-037e1206bc9c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 21,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 124,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "73177181-310b-478f-9b9e-b0f5bc108fcf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 21,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 117,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "6dfb9475-e4fd-4c29-b630-70c866eb1b63",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 21,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 105,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "211c86d8-5a4c-4fa5-ac99-2bbabc2ce1d5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 21,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 98,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "77fdbc35-c488-487e-9cbf-7e3c411d83a8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 21,
                "offset": 1,
                "shift": 8,
                "w": 4,
                "x": 92,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "056d5a07-3c4e-48ed-83db-1cf1859207a0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 21,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 82,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "91456eb2-b163-457f-9ca1-26f2ffc4e1cb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 21,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 71,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "042cdbbe-c04d-48a0-b46b-292db92b9261",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 21,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 61,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "d136f7da-420c-425a-896b-27aca4690a09",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 21,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 51,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "409e1cfe-16f5-4136-bff0-c878986f5d38",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 21,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 40,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "0a47273b-5fb7-4ba3-8997-77989cfc0d16",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 21,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 32,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "2f44e412-c998-492d-b633-a13f827eb59e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 21,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 22,
                "y": 2
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "839acfb0-42bb-4882-a864-815cc5619e16",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 21,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 12,
                "y": 2
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "f772a579-0dda-48a7-920f-405c162aa74d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 21,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 7,
                "y": 2
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "cb6ea3dc-7667-4f7d-a463-52561ca95ec1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 21,
                "offset": -2,
                "shift": 4,
                "w": 5,
                "x": 214,
                "y": 2
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "7fb8df3d-332c-484d-824b-9fcaeb0c2c62",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 21,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 221,
                "y": 2
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "832c6530-7ed5-4b34-9b66-ba118d505f15",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 21,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 231,
                "y": 2
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "c2f1114a-bdba-45df-9e9d-d9084b444a2c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 21,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 235,
                "y": 2
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "2e8f4a7c-d801-4476-9e6a-c1402f3c5194",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 21,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 203,
                "y": 25
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "5bd193e9-5d7c-473d-bc7e-abcf9b72077a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 21,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 192,
                "y": 25
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "9c41abbf-295e-466d-81b3-c95fe11921d7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 21,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 181,
                "y": 25
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "347d7864-013d-4581-88c0-e890aca5c007",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 21,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 171,
                "y": 25
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "97a015d4-aadc-4254-b329-8f86225e2a81",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 21,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 164,
                "y": 25
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "5a8a983d-e65e-408f-94c0-abad2bc7bfdd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 21,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 154,
                "y": 25
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "00c4b13a-fef7-4824-8156-63b26307294e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 21,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 146,
                "y": 25
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "b83d3402-b8fd-4fba-88b9-e2a4b7ca280f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 21,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 136,
                "y": 25
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "c2a9abd2-e7f0-48bd-86f2-1973d8fb8ffb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 21,
                "offset": 0,
                "shift": 8,
                "w": 9,
                "x": 125,
                "y": 25
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "facecd02-0119-4af6-b227-abe4bf9296d4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 21,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 111,
                "y": 25
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "d16c1f47-2a53-4423-b230-c957090e4fda",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 21,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 213,
                "y": 25
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "c52e5398-9863-4c67-bd32-a82eb430fc8d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 21,
                "offset": 0,
                "shift": 8,
                "w": 9,
                "x": 100,
                "y": 25
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "39f02d5b-0ce7-46f1-94cd-61094d503630",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 21,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 79,
                "y": 25
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "7e99d6ce-6653-40b9-9ccf-d7d8f4338953",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 21,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 72,
                "y": 25
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "cbd12b89-d950-4dcd-b20b-41a89f387746",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 21,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 67,
                "y": 25
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "7a9a9d69-91bb-4334-8cb5-e13878ed50dc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 21,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 60,
                "y": 25
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "72bddcf4-4592-4a48-b570-a3c00db4da03",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 21,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 48,
                "y": 25
            }
        },
        {
            "Key": 215,
            "Value": {
                "id": "94068102-4e3e-4a3f-ab5b-19ea0c97eb89",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 215,
                "h": 21,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 36,
                "y": 25
            }
        },
        {
            "Key": 228,
            "Value": {
                "id": "b0920549-5229-4a6f-8e92-db1349305652",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 228,
                "h": 21,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 26,
                "y": 25
            }
        },
        {
            "Key": 8226,
            "Value": {
                "id": "33bbc45c-6097-48f2-92f0-e84fb35c754a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8226,
                "h": 21,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 19,
                "y": 25
            }
        },
        {
            "Key": 8364,
            "Value": {
                "id": "956d40a4-d68d-4ba0-8a85-6a5b29f2487b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8364,
                "h": 21,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 8,
                "y": 25
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "baa1e585-22dd-4c0e-8f7b-e5df38f6abb4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 21,
                "offset": 3,
                "shift": 10,
                "w": 4,
                "x": 2,
                "y": 25
            }
        },
        {
            "Key": 9650,
            "Value": {
                "id": "ca6e41e4-0610-4fdf-90c2-4e357e65c16a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9650,
                "h": 21,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 233,
                "y": 48
            }
        },
        {
            "Key": 9660,
            "Value": {
                "id": "756fbe68-1818-4f2e-8b82-99f49c62801d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9660,
                "h": 21,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 12,
                "y": 94
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 215,
            "y": 215
        },
        {
            "x": 228,
            "y": 228
        },
        {
            "x": 8226,
            "y": 8226
        },
        {
            "x": 8364,
            "y": 8364
        },
        {
            "x": 9647,
            "y": 9647
        },
        {
            "x": 9650,
            "y": 9650
        },
        {
            "x": 9660,
            "y": 9660
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)\\u000a×",
    "size": 12,
    "styleName": "Medium",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}