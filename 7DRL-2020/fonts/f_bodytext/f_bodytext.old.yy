{
    "id": "44840b42-92cd-4839-9111-0aa4ba2f0d73",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "f_bodytext",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 0,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Cooper Hewitt",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "a0410edb-f21b-494a-aee9-86b17745943e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 21,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "b5d1a289-50c0-4020-bc34-61e670e315f2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 21,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 204,
                "y": 48
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "6a6b87b5-5a5b-4dad-9800-0b1704a7cb65",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 21,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 196,
                "y": 48
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "993fdf45-c8cd-4f67-859b-ee1bd2a49495",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 21,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 184,
                "y": 48
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "13e89ab4-f70e-4a98-b537-41b8f2065d2f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 21,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 173,
                "y": 48
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "fe5823ca-5303-45de-b933-dd1a478f0058",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 21,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 156,
                "y": 48
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "7405daea-93ac-42b4-a11d-b6376e6fdf1c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 21,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 143,
                "y": 48
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "d3b61ac8-5c9f-4a25-899f-831f055c2ebe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 21,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 138,
                "y": 48
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "c123ff6d-d8d6-492c-a0cc-18b4a9c21e2f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 21,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 131,
                "y": 48
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "9cafa3f4-7b2c-4420-9f47-fd62ff90790a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 21,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 124,
                "y": 48
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "55e2253f-9537-47e6-b797-f5dab8825e19",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 21,
                "offset": 0,
                "shift": 6,
                "w": 7,
                "x": 115,
                "y": 48
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "0ed01186-21fe-4cf7-bb93-2fc708329e0c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 21,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 104,
                "y": 48
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "c9217c98-1eb7-4441-81d3-fc800808eb93",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 21,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 99,
                "y": 48
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "88db0786-c6c8-4dc4-a419-d0fb5a940b3d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 21,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 92,
                "y": 48
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "021e6d7e-d6ba-4fde-9d3c-350b553a1540",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 21,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 87,
                "y": 48
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "52f307a6-0fe1-45a4-868a-31a7851a4a57",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 21,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 79,
                "y": 48
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "993d2d08-bb78-454d-a0e0-80db0e3c377c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 21,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 68,
                "y": 48
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "eb18050d-c26b-4166-a056-2ed28ddf565a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 21,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 57,
                "y": 48
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "167c6406-931e-42e6-b785-153942724652",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 21,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 46,
                "y": 48
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "fa743189-8536-4b3b-8057-428f246d903b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 21,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 35,
                "y": 48
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "7202a9a9-7933-4a61-9564-9ccaf873dc30",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 21,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 23,
                "y": 48
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "2fffccb1-5c36-42e1-8688-ff4bd40f09f0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 21,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 12,
                "y": 48
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "9985fa7d-da54-4941-98e9-70207373edd0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 21,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 209,
                "y": 48
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "42c098d6-97bf-40ef-af40-e1c0c521cf9b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 21,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 220,
                "y": 48
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "31da5045-a183-4efd-b35c-6ff59aebbe88",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 21,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 231,
                "y": 48
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "4080f32d-7b86-433e-b6f5-87f46d9d922a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 21,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 242,
                "y": 48
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "dd63041e-1f49-4762-8086-e40e8c9da9e6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 21,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 227,
                "y": 71
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "15143241-a13a-4789-8768-43413b98d564",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 21,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 222,
                "y": 71
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "b1f1343e-7053-4721-bf20-887b9efbb002",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 21,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 211,
                "y": 71
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "bbbd1d68-0ef0-44e4-8876-6518baf52db5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 21,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 200,
                "y": 71
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "d98a181c-e6d1-4d55-aadc-d5655e05fb88",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 21,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 189,
                "y": 71
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "3218f5d0-915f-4995-9bf8-a0fdc77de574",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 21,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 179,
                "y": 71
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "b1299dbc-49f1-496b-bb92-1ffd2e403e3b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 21,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 161,
                "y": 71
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "e2e1de20-d566-44e9-88ea-35686c74ce5e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 21,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 149,
                "y": 71
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "90141133-96b1-4725-9e08-c0b38b2c7f19",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 139,
                "y": 71
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "b76a6b85-403d-43f5-abcf-a99faefa6d25",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 21,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 127,
                "y": 71
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "8bbd0d6d-1686-45f1-8924-12b2c86bbb85",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 232,
                "y": 71
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "8ae696db-9d48-4601-aa7c-6543d2d1f38e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 21,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 117,
                "y": 71
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "ed5f1fdf-d8c7-4b8b-ac13-fb6ca79503bc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 21,
                "offset": 1,
                "shift": 8,
                "w": 8,
                "x": 95,
                "y": 71
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "bb2f5c80-ef08-4657-8e1f-03b3b9568812",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 21,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 83,
                "y": 71
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "cf0a50bf-b1a3-4472-be9c-487fd7fd1959",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 72,
                "y": 71
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "2446f3c5-d930-4661-9505-99e19fda4f3a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 21,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 68,
                "y": 71
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "af7b0c38-1203-4d80-a794-cd1d418b77a5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 21,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 58,
                "y": 71
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "889809c4-242a-4acf-9dab-bb30777c9d59",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 21,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 48,
                "y": 71
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "2b1cfa78-9b7d-40c9-8ab0-52c679e9c031",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 21,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 39,
                "y": 71
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "26cbc5d5-cf8a-4de8-abda-ffc566d2b2e0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 21,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 25,
                "y": 71
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "276e69ae-ae02-4924-8b96-795534ba5252",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 21,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 14,
                "y": 71
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "5ad75097-1d78-4299-879a-c54534e97263",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 21,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 2,
                "y": 71
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "da8b9ee5-e5df-4f53-9b7c-9fff8b4be816",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 21,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 2,
                "y": 48
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "3d628719-e99f-4c7a-af2c-76a481447711",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 21,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 105,
                "y": 71
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "e787be44-28e2-4008-b0b4-2b0f9b4ec9ab",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 21,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 238,
                "y": 25
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "31f656f9-56c8-4152-a3ac-591d2348fdf3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 21,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 84,
                "y": 25
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "4209a1ac-5b53-4fc4-b330-f2539345ad2e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 21,
                "offset": 0,
                "shift": 8,
                "w": 9,
                "x": 209,
                "y": 2
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "531b6036-527e-4b10-a945-a0d591be4ce7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 21,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 197,
                "y": 2
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "6b10fb09-d653-4a31-b2a7-668df49403f2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 21,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 185,
                "y": 2
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "53b45944-b97f-4354-ab7c-0b1206023d81",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 21,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 168,
                "y": 2
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "ccdc54c0-44d6-4c5e-9dc3-5f13e3f6e3ac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 21,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 156,
                "y": 2
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "c011b0df-e85c-4ff1-bcf5-2f890a3a8eb6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 21,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 144,
                "y": 2
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "993559d2-0308-4cc4-aa86-63e4abc3a903",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 21,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 133,
                "y": 2
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "fdb88bcd-8822-4949-aa2b-f26f4f4a5f44",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 21,
                "offset": 1,
                "shift": 5,
                "w": 4,
                "x": 127,
                "y": 2
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "9601a441-df9f-4db6-817d-914599cb9453",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 21,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 119,
                "y": 2
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "5dc5303d-5fc6-4137-95a4-e82546a2da41",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 21,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 112,
                "y": 2
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "f45cdb71-01b7-4839-931c-781b6eef87cc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 21,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 100,
                "y": 2
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "150dc130-4085-451e-b65d-09abb7d18aff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 21,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 93,
                "y": 2
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "a4bd2a9d-d9c8-4560-ab9e-3fb6de977428",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 21,
                "offset": 1,
                "shift": 8,
                "w": 4,
                "x": 87,
                "y": 2
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "9f3a795f-d489-4040-a3b9-65ca7f766397",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 21,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 77,
                "y": 2
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "0a2d2ecf-0aa8-45bf-9bb9-bdf18c15f667",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 21,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 66,
                "y": 2
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "95915f97-0924-48dd-a0e4-907d1cab75b8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 21,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 56,
                "y": 2
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "63044612-0bd5-4412-99fb-07d8c14c0c21",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 21,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 46,
                "y": 2
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "219e22c4-2cfe-4166-8254-bf7d63e766ae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 21,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 35,
                "y": 2
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "6459d725-52f3-4b6b-bde2-07d3463b8904",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 21,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 27,
                "y": 2
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "ce8e5dd9-a99f-4731-9c97-06e9ae1b77be",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 21,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 17,
                "y": 2
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "b823ab92-402b-4a37-9589-edb67bbfbb0b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 21,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 7,
                "y": 2
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "19902da3-b1bc-4d89-a32d-513e2da56824",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 21,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 220,
                "y": 2
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "9ea4af67-c49f-43fd-a2f6-ed18c9c3db94",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 21,
                "offset": -2,
                "shift": 4,
                "w": 5,
                "x": 225,
                "y": 2
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "2d72a7e4-3b2f-4041-a8ab-cd78015eecc3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 21,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 232,
                "y": 2
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "64d31129-bf29-446d-822c-d1ee6c9d318a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 21,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 242,
                "y": 2
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "2ea2149f-b308-446d-94c9-890f9c276379",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 21,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 194,
                "y": 25
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "05218db9-1dc6-450e-ba49-f9a38072b614",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 21,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 184,
                "y": 25
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "f93aec78-9454-466e-870d-2f8ca4639e8b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 21,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 173,
                "y": 25
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "a288509e-0d05-4129-ac32-60e48454296e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 21,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 162,
                "y": 25
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "ff4cead2-2bd2-4350-ac7e-f6837a4cf821",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 21,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 152,
                "y": 25
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "4b4930c5-1f47-4710-ade0-c506ae2d3732",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 21,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 145,
                "y": 25
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "ed4cf857-78a3-4ff8-b0f0-94525a6a6d57",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 21,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 135,
                "y": 25
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "b5b73fca-1cbd-4c40-9f20-3e28748474ad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 21,
                "offset": 0,
                "shift": 7,
                "w": 6,
                "x": 127,
                "y": 25
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "0af5f524-4fc1-4f96-8fdc-a27224dc59ff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 21,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 117,
                "y": 25
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "e77e81dd-993f-463d-b4b4-70ebd6c7c692",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 21,
                "offset": 0,
                "shift": 8,
                "w": 9,
                "x": 106,
                "y": 25
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "7594c832-70b7-46db-8a35-7bee7f331442",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 21,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 209,
                "y": 25
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "837eaf13-e653-49a2-8c8c-b62cdebfaa3b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 21,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 96,
                "y": 25
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "46847ec1-a3c7-435c-9d90-286a4658c60d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 21,
                "offset": 0,
                "shift": 8,
                "w": 9,
                "x": 73,
                "y": 25
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "db66d3d6-32b9-47f4-9c0e-6385fbb14a15",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 21,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 63,
                "y": 25
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "fde36369-ed8a-4527-9ffd-f9f295b90283",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 21,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 56,
                "y": 25
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "51399f0e-a60b-456c-9a46-9ad529b5dbe1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 21,
                "offset": 0,
                "shift": 4,
                "w": 3,
                "x": 51,
                "y": 25
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "af9e7597-1451-46d7-98ea-b86624586fea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 21,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 44,
                "y": 25
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "3b459b45-412b-4f79-904a-8c28de12abc5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 21,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 32,
                "y": 25
            }
        },
        {
            "Key": 215,
            "Value": {
                "id": "54166084-7579-4203-8361-025e5f3e9c3f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 215,
                "h": 21,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 20,
                "y": 25
            }
        },
        {
            "Key": 8226,
            "Value": {
                "id": "a3392249-057e-4afd-875b-e8d0b1a26427",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8226,
                "h": 21,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 13,
                "y": 25
            }
        },
        {
            "Key": 8364,
            "Value": {
                "id": "231089fc-9321-4105-b519-d2defb06c74c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 8364,
                "h": 21,
                "offset": 0,
                "shift": 10,
                "w": 9,
                "x": 2,
                "y": 25
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "28d4e51e-98d4-4eda-8ba5-69b3c2599e46",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 21,
                "offset": 3,
                "shift": 10,
                "w": 4,
                "x": 246,
                "y": 2
            }
        },
        {
            "Key": 9650,
            "Value": {
                "id": "3016382d-291a-4118-9e16-cf6b3f5ef69b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9650,
                "h": 21,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 223,
                "y": 25
            }
        },
        {
            "Key": 9660,
            "Value": {
                "id": "98ee4a35-96d0-4dc5-8307-61cc18dfa4e8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9660,
                "h": 21,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 2,
                "y": 94
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 127
        },
        {
            "x": 215,
            "y": 215
        },
        {
            "x": 8226,
            "y": 8226
        },
        {
            "x": 8364,
            "y": 8364
        },
        {
            "x": 9647,
            "y": 9647
        },
        {
            "x": 9650,
            "y": 9650
        },
        {
            "x": 9660,
            "y": 9660
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)\\u000a×",
    "size": 12,
    "styleName": "Medium",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}