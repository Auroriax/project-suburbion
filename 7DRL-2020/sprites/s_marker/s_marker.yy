{
    "id": "1d2f8a03-2ed5-4874-8bbb-dcb0140e27b0",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_marker",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 31,
    "bbox_top": 0,
    "bboxmode": 2,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d8bb7463-790b-4a54-8edf-f279b197eba5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1d2f8a03-2ed5-4874-8bbb-dcb0140e27b0",
            "compositeImage": {
                "id": "282911ce-c885-4166-a8e4-6465d2547405",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d8bb7463-790b-4a54-8edf-f279b197eba5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d3e2f170-31fc-4292-a6ec-d1cce573580b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d8bb7463-790b-4a54-8edf-f279b197eba5",
                    "LayerId": "2815aa8f-6bb6-4ce7-b22b-daa6ea4a86f8"
                }
            ]
        },
        {
            "id": "54d6ed75-f3b0-4542-8185-63360a926c47",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1d2f8a03-2ed5-4874-8bbb-dcb0140e27b0",
            "compositeImage": {
                "id": "5b748021-0304-46b5-b6bc-ba4db4a68a2d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "54d6ed75-f3b0-4542-8185-63360a926c47",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3cd961cf-fed0-4bd1-a490-405b7b513af3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "54d6ed75-f3b0-4542-8185-63360a926c47",
                    "LayerId": "2815aa8f-6bb6-4ce7-b22b-daa6ea4a86f8"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "2815aa8f-6bb6-4ce7-b22b-daa6ea4a86f8",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1d2f8a03-2ed5-4874-8bbb-dcb0140e27b0",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 32,
    "xorig": 5,
    "yorig": 24
}