{
    "id": "3cd357e8-07c8-4bc3-bb14-76541527f80e",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_donut",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 7,
    "bbox_left": 0,
    "bbox_right": 7,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "39ddba92-0002-4fe3-b584-c96ff14d1d9d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3cd357e8-07c8-4bc3-bb14-76541527f80e",
            "compositeImage": {
                "id": "b4a9d613-177f-470f-91c6-a81a546d30e7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "39ddba92-0002-4fe3-b584-c96ff14d1d9d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dcac4695-8797-497c-bc0d-796c57746ffd",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "39ddba92-0002-4fe3-b584-c96ff14d1d9d",
                    "LayerId": "ec5b8312-958e-43f9-a3be-b7d53f399a7d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 8,
    "layers": [
        {
            "id": "ec5b8312-958e-43f9-a3be-b7d53f399a7d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3cd357e8-07c8-4bc3-bb14-76541527f80e",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 8,
    "xorig": 0,
    "yorig": 0
}