{
    "id": "00755605-8a65-40ac-88bf-9882366dd9a7",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_currentnode",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 62,
    "bbox_left": 18,
    "bbox_right": 47,
    "bbox_top": 2,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "ac35fd42-e51a-4a84-8c20-e705eadcfe2e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "00755605-8a65-40ac-88bf-9882366dd9a7",
            "compositeImage": {
                "id": "db63145e-8e35-4816-b32b-df60a69b3937",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ac35fd42-e51a-4a84-8c20-e705eadcfe2e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4b470330-a01b-4a68-976e-579125a24c2f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ac35fd42-e51a-4a84-8c20-e705eadcfe2e",
                    "LayerId": "8f00f24b-cf66-4e9b-9f13-6782d07641c9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "8f00f24b-cf66-4e9b-9f13-6782d07641c9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "00755605-8a65-40ac-88bf-9882366dd9a7",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}