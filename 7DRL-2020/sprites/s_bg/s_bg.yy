{
    "id": "e3651012-11f6-44c3-8897-35dd1abec7b9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_bg",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 579,
    "bbox_left": 0,
    "bbox_right": 799,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "605d9dbc-af50-46b8-99c1-c800769f8814",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e3651012-11f6-44c3-8897-35dd1abec7b9",
            "compositeImage": {
                "id": "6bc7fa16-dac4-44e5-8ddc-0a5a0fc4726b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "605d9dbc-af50-46b8-99c1-c800769f8814",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "1b8cc158-e1c2-4944-8864-02f1e1b7b14c",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "605d9dbc-af50-46b8-99c1-c800769f8814",
                    "LayerId": "ccf0d171-2314-4ff7-a689-9175cd9f7aba"
                }
            ]
        },
        {
            "id": "418b5758-7c88-4d1f-bfe3-cf7526b7bd3c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e3651012-11f6-44c3-8897-35dd1abec7b9",
            "compositeImage": {
                "id": "b2edcd41-d943-488c-a5b9-094a79a30592",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "418b5758-7c88-4d1f-bfe3-cf7526b7bd3c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "531645ca-6cc2-44cf-8ff3-3b74d521cd4f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "418b5758-7c88-4d1f-bfe3-cf7526b7bd3c",
                    "LayerId": "ccf0d171-2314-4ff7-a689-9175cd9f7aba"
                }
            ]
        },
        {
            "id": "ac2be4d8-14e0-4149-b539-812a20f62c60",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e3651012-11f6-44c3-8897-35dd1abec7b9",
            "compositeImage": {
                "id": "6539687a-a55d-4881-9296-df426cdbdd1b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "ac2be4d8-14e0-4149-b539-812a20f62c60",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "4cf0c60d-fde8-40e4-a837-fb5f50691518",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "ac2be4d8-14e0-4149-b539-812a20f62c60",
                    "LayerId": "ccf0d171-2314-4ff7-a689-9175cd9f7aba"
                }
            ]
        },
        {
            "id": "b329069a-57c7-4e49-8046-19c40a3f2057",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e3651012-11f6-44c3-8897-35dd1abec7b9",
            "compositeImage": {
                "id": "23337a4e-cfaf-4e15-9836-1f7ff71eb500",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b329069a-57c7-4e49-8046-19c40a3f2057",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3fe91a49-87e7-4099-9509-d7090101a516",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b329069a-57c7-4e49-8046-19c40a3f2057",
                    "LayerId": "ccf0d171-2314-4ff7-a689-9175cd9f7aba"
                }
            ]
        },
        {
            "id": "aacb2378-d1a5-41da-861c-122d1b6e72df",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e3651012-11f6-44c3-8897-35dd1abec7b9",
            "compositeImage": {
                "id": "b45cb660-e9eb-47c3-81d3-64a2b8d74731",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "aacb2378-d1a5-41da-861c-122d1b6e72df",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "edef0942-4128-4edd-8c8b-5bc51245f102",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "aacb2378-d1a5-41da-861c-122d1b6e72df",
                    "LayerId": "ccf0d171-2314-4ff7-a689-9175cd9f7aba"
                }
            ]
        },
        {
            "id": "cbb1d22a-3f52-4720-ace0-c49d2b76dcdf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e3651012-11f6-44c3-8897-35dd1abec7b9",
            "compositeImage": {
                "id": "9a0f3ccf-6305-42c6-b137-158679f3dbd0",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cbb1d22a-3f52-4720-ace0-c49d2b76dcdf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5108fabb-ac25-44c8-89b2-b1c27bff0c12",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cbb1d22a-3f52-4720-ace0-c49d2b76dcdf",
                    "LayerId": "ccf0d171-2314-4ff7-a689-9175cd9f7aba"
                }
            ]
        },
        {
            "id": "4ee21c47-a26c-4358-b09e-af97ab0cf66d",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e3651012-11f6-44c3-8897-35dd1abec7b9",
            "compositeImage": {
                "id": "c5cf0810-4a00-4edf-9dea-1787ce13ffc8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4ee21c47-a26c-4358-b09e-af97ab0cf66d",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "437ff1bb-6e7c-42e3-b6ea-4f9366e0d77d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4ee21c47-a26c-4358-b09e-af97ab0cf66d",
                    "LayerId": "ccf0d171-2314-4ff7-a689-9175cd9f7aba"
                }
            ]
        },
        {
            "id": "a8751c02-b4f6-4402-809c-123440415905",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e3651012-11f6-44c3-8897-35dd1abec7b9",
            "compositeImage": {
                "id": "231e33f3-f377-4137-bcca-9f6477343ab9",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "a8751c02-b4f6-4402-809c-123440415905",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bd137b66-4236-4afa-b28e-468b2baeacaa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "a8751c02-b4f6-4402-809c-123440415905",
                    "LayerId": "ccf0d171-2314-4ff7-a689-9175cd9f7aba"
                }
            ]
        },
        {
            "id": "35da55ae-dfd0-4dd6-97dd-03716d24b78a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e3651012-11f6-44c3-8897-35dd1abec7b9",
            "compositeImage": {
                "id": "31c72db2-dc07-44c7-b38d-59f102559427",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "35da55ae-dfd0-4dd6-97dd-03716d24b78a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "697afbaf-a3b8-4110-979a-24349dbc483d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "35da55ae-dfd0-4dd6-97dd-03716d24b78a",
                    "LayerId": "ccf0d171-2314-4ff7-a689-9175cd9f7aba"
                }
            ]
        },
        {
            "id": "d0147c46-54e4-45d1-b883-f6a859c460ae",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e3651012-11f6-44c3-8897-35dd1abec7b9",
            "compositeImage": {
                "id": "75c391d6-f3d8-46c9-9668-35230a99f8c1",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d0147c46-54e4-45d1-b883-f6a859c460ae",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f423c813-516c-488e-b12b-7e92cad1deb3",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d0147c46-54e4-45d1-b883-f6a859c460ae",
                    "LayerId": "ccf0d171-2314-4ff7-a689-9175cd9f7aba"
                }
            ]
        },
        {
            "id": "17f2c9bf-23a9-4d6e-88f7-80734fe3f1a0",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e3651012-11f6-44c3-8897-35dd1abec7b9",
            "compositeImage": {
                "id": "0c4aaa53-0f6e-434a-be0d-8116b9f87f89",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "17f2c9bf-23a9-4d6e-88f7-80734fe3f1a0",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d66e9640-c986-48e2-b99b-2317daf4bfac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "17f2c9bf-23a9-4d6e-88f7-80734fe3f1a0",
                    "LayerId": "ccf0d171-2314-4ff7-a689-9175cd9f7aba"
                }
            ]
        },
        {
            "id": "b31f4bec-ddc6-409b-871a-77cf24d21e68",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "e3651012-11f6-44c3-8897-35dd1abec7b9",
            "compositeImage": {
                "id": "f20023ba-9324-4257-8ae6-21e66d5db777",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b31f4bec-ddc6-409b-871a-77cf24d21e68",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "38f12058-9327-4f7f-8a90-03463eed39ab",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b31f4bec-ddc6-409b-871a-77cf24d21e68",
                    "LayerId": "ccf0d171-2314-4ff7-a689-9175cd9f7aba"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 580,
    "layers": [
        {
            "id": "ccf0d171-2314-4ff7-a689-9175cd9f7aba",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "e3651012-11f6-44c3-8897-35dd1abec7b9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 800,
    "xorig": 400,
    "yorig": 290
}