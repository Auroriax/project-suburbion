{
    "id": "236220a7-d450-4307-b415-a680d5447634",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "_7drl_icon",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 63,
    "bbox_left": 0,
    "bbox_right": 63,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "435b9b65-1aa0-48db-8146-e694de84a301",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "236220a7-d450-4307-b415-a680d5447634",
            "compositeImage": {
                "id": "035c4e6f-3916-4330-b05a-23ec443b309a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "435b9b65-1aa0-48db-8146-e694de84a301",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d3979f1b-89da-4607-a4b8-ec715a1b632f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "435b9b65-1aa0-48db-8146-e694de84a301",
                    "LayerId": "6d4efeb7-d77c-404c-b0cb-a6146b4c3e9d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 64,
    "layers": [
        {
            "id": "6d4efeb7-d77c-404c-b0cb-a6146b4c3e9d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "236220a7-d450-4307-b415-a680d5447634",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 64,
    "xorig": 0,
    "yorig": 0
}