{
    "id": "d7d8ac45-bcf8-47e6-9cf2-80541027df44",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_nope",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 46,
    "bbox_left": 1,
    "bbox_right": 46,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "2e1cf839-3bff-4529-8be3-4e7bd4136aa9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "d7d8ac45-bcf8-47e6-9cf2-80541027df44",
            "compositeImage": {
                "id": "5f9ced4f-3ed1-452a-888b-a719bc666136",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2e1cf839-3bff-4529-8be3-4e7bd4136aa9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "3d170001-75a3-4f7b-90c4-fe72b5b61263",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2e1cf839-3bff-4529-8be3-4e7bd4136aa9",
                    "LayerId": "15c71ab1-1eab-4c61-b8c2-829e93b2303c"
                }
            ]
        }
    ],
    "gridX": 1,
    "gridY": 1,
    "height": 48,
    "layers": [
        {
            "id": "15c71ab1-1eab-4c61-b8c2-829e93b2303c",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "d7d8ac45-bcf8-47e6-9cf2-80541027df44",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 48,
    "xorig": 24,
    "yorig": 24
}