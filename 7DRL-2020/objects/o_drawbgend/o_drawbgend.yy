{
    "id": "a4ef074e-9800-4c1b-8a1a-b3eeefdd6ee3",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_drawbgend",
    "eventList": [
        {
            "id": "e7e76449-b59d-4785-b81f-c7b8b4f18339",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "a4ef074e-9800-4c1b-8a1a-b3eeefdd6ee3"
        },
        {
            "id": "d5b80306-cb5d-4a3f-ac45-5a4bd61df3f5",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "a4ef074e-9800-4c1b-8a1a-b3eeefdd6ee3"
        },
        {
            "id": "0177cd3d-e0c1-4e59-a3fe-8b77e30e4ba4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "a4ef074e-9800-4c1b-8a1a-b3eeefdd6ee3"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}