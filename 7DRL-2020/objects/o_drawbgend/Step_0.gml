/// @description QQQ

if mouse_check_button(mb_left)
{
	held += 1
	
	if held == 1
	{
		floattext(mouse_x,mouse_y,"Restarting game...")
	}
	
	if held >= hold
	{
		randomize()
		mouse_clear(mb_left);
		audio_stop_all();
		room_goto(Game);
	}
}
else
	held = 0