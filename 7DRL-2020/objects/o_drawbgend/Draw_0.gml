/// @description QQQ

draw_sprite_ext(sprite_index,image_index,x,y,1,1,0,-1,image_alpha)

draw_graph(netvaluegraph,goal,100,100,room_width-200,room_height-200,25,c_gray)

draw_set_valign(1); draw_set_halign(1);
var txt = "Game over!";
if money > goal {txt = "You win!"}
draw_set_font(f_bodytextbold)
draw_textlabel(room_width/2,40,txt,c_white,c_black);

draw_set_font(f_bodytext)
var txt = "You didn't reach the goal of €"+string(goal) + " within "+string(limit) + " minutes. Try again!";
if money > goal {txt = "You achieved the goal of €"+string(goal) + " within "+string(time)+ " minutes. Great job!";}
draw_textlabel(room_width/2,70,txt,c_white,c_black);

draw_pie(mouse_x,mouse_y,held,hold,c_red,15,1,90)