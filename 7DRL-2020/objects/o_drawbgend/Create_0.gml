/// @description QQQ

if global.winsprite != noone && sprite_exists(global.winsprite)
sprite_index = global.winsprite;

hold = 120;
held = 0;

image_alpha = 0.4;

gpu_set_blendenable(true);

mouse_clear(mb_left);