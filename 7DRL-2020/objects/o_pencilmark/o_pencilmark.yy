{
    "id": "516033ad-8ac3-4080-a1ad-04c2a2b1135a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_pencilmark",
    "eventList": [
        {
            "id": "a9227661-1ba3-4313-b382-02b4b6056169",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "516033ad-8ac3-4080-a1ad-04c2a2b1135a"
        },
        {
            "id": "3d680743-ea1d-430d-b534-3aac5b956bf4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 10,
            "eventtype": 6,
            "m_owner": "516033ad-8ac3-4080-a1ad-04c2a2b1135a"
        },
        {
            "id": "4c087c12-22a4-4bef-8429-90c2637fbe69",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 11,
            "eventtype": 6,
            "m_owner": "516033ad-8ac3-4080-a1ad-04c2a2b1135a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "1d2f8a03-2ed5-4874-8bbb-dcb0140e27b0",
    "visible": true
}