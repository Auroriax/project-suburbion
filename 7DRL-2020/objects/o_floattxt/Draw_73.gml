/// @description QQQ

draw_set_halign(1); draw_set_valign(1);
draw_set_font(f_bodytext)
var tempalph = min(1,alph)

draw_textoutline(round(x),round(y),txt,c_white,-1,tempalph)
var c = c_black;
draw_text_color(round(x),round(y),txt,c,c,c,c,tempalph)
draw_set_halign(0); draw_set_valign(0);