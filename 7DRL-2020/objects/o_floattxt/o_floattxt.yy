{
    "id": "89d6d5ad-84eb-4743-89ea-2221a765da29",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_floattxt",
    "eventList": [
        {
            "id": "39190753-5646-4bd2-abb8-8e58fb724e10",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "89d6d5ad-84eb-4743-89ea-2221a765da29"
        },
        {
            "id": "28df3c2e-23b9-4e9d-a8cd-c98df5176a7c",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 73,
            "eventtype": 8,
            "m_owner": "89d6d5ad-84eb-4743-89ea-2221a765da29"
        },
        {
            "id": "30c34ce2-3b5f-4d13-a3d1-41689d763035",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "89d6d5ad-84eb-4743-89ea-2221a765da29"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}