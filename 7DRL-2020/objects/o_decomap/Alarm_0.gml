/// @description Late init

image_speed = 0;
image_index = irandom(image_number)
image_alpha = random_range(0.05,0.1);

x += irandom_range(-100,100)
y += irandom_range(-50,50)

anim = random(loop);
range = random_range(14,16);
inc = random_range(0.006,0.006)

image_angle = choose(0,90,180,270)
image_xscale = choose(-1,1)
image_yscale = choose(-1,1)