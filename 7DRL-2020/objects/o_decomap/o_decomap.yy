{
    "id": "9e8ef98c-6389-49ec-a041-847c06b8a94b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_decomap",
    "eventList": [
        {
            "id": "dba06b31-aaef-4011-8cf4-0548081bc535",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "9e8ef98c-6389-49ec-a041-847c06b8a94b"
        },
        {
            "id": "42bfc5a8-7c96-44e4-b57f-6e4fcf9d43ef",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "9e8ef98c-6389-49ec-a041-847c06b8a94b"
        },
        {
            "id": "ee70b38f-4e30-440a-9c8a-12e983e36d24",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "9e8ef98c-6389-49ec-a041-847c06b8a94b"
        },
        {
            "id": "1b958cab-812d-4133-9023-e359a0088e27",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "9e8ef98c-6389-49ec-a041-847c06b8a94b"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "e3651012-11f6-44c3-8897-35dd1abec7b9",
    "visible": true
}