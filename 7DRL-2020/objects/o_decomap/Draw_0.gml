/// @description Groopity noopity!

gpu_set_blendenable(true);

anim += inc;
if anim > loop {anim -= loop}

draw_sprite_ext(sprite_index,image_index,x,y+(sin(anim)*range),image_xscale,image_yscale,image_angle,-1,image_alpha)