{
    "id": "cbf9869d-17ac-49c2-8c48-514bfed535a1",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_welcome",
    "eventList": [
        {
            "id": "2defbe0e-9936-42b2-97c8-4a6c0689d097",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 73,
            "eventtype": 8,
            "m_owner": "cbf9869d-17ac-49c2-8c48-514bfed535a1"
        },
        {
            "id": "a3a1cc69-84b5-4c4e-8a3f-23b2b2dfbf5d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 50,
            "eventtype": 6,
            "m_owner": "cbf9869d-17ac-49c2-8c48-514bfed535a1"
        },
        {
            "id": "5e03e7d0-3a0f-4739-b794-fde5f8a0a5ed",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "cbf9869d-17ac-49c2-8c48-514bfed535a1"
        },
        {
            "id": "a03fb3b5-d786-4814-8e67-6ef55723ea92",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 2,
            "eventtype": 7,
            "m_owner": "cbf9869d-17ac-49c2-8c48-514bfed535a1"
        },
        {
            "id": "44f829f3-01cf-450d-89a2-a338dfa0c732",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 1,
            "m_owner": "cbf9869d-17ac-49c2-8c48-514bfed535a1"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}