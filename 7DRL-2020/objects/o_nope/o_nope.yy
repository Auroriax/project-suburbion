{
    "id": "9434cb06-c92d-44e2-a993-c62b5dad8682",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_nope",
    "eventList": [
        {
            "id": "4ce3e12f-ad2d-4a79-b86f-c23a8073a64e",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "9434cb06-c92d-44e2-a993-c62b5dad8682"
        },
        {
            "id": "76c4e115-a154-4a9d-82ce-767aa11e0dfc",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "9434cb06-c92d-44e2-a993-c62b5dad8682"
        },
        {
            "id": "4e3fd67b-e9ea-4a0c-aa96-5b0a34c2bf42",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 73,
            "eventtype": 8,
            "m_owner": "9434cb06-c92d-44e2-a993-c62b5dad8682"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "d7d8ac45-bcf8-47e6-9cf2-80541027df44",
    "visible": true
}