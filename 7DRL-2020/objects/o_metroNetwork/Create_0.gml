/// @description Generate metro network

audio_play_sound(a_music,0,true);

seed = random_get_seed();

w = 9; h = 6;
network = []
for(var i = 0; i != w; i += 1)
{
	for(var j = 0; j != w; j += 1)
	{
		network[i , j ] = MAP_EMPTY
	}
}

nodes = [];

var xo = round(w/2); var yo = round(h/2)
network[ xo , yo ] = 1;
var ins = node_define(1, xo, yo);
ins.visited = true;
ins.revealed = true;
network = map_markborder(network, xo, yo);

var maxx = xo; var maxy = yo;
var minx = xo; var miny = yo;

//Create nodes
var amount = 16 //irandom_range(7,11);
for(var z = 1; z != amount; z += 1)
{
	var candidates = [];
	
	//Graph outline of map
	for(var i = 0; i != w; i += 1)
	{
		for(var j = 0; j != h; j += 1)
		{
			console_log(i,":",j," ",network[i,j])
			if (network[i,j] == MAP_BORDER)
			{
				show_debug_message("Found candidate at "+string(i)+"|"+string(j))
				var pos = [i, j]
				candidates = array_push(candidates, pos);
			}
		}
	}
	
	var rarray = array_random(candidates);
	console_log(rarray)
	var ax = real(rarray[0]); var ay = real(rarray[1]);
	network[ax, ay ] = z + 1;
	network = map_markborder(network, ax , ay);
	node_define(z + 1, ax, ay);
	
	if ax > maxx {maxx = ax}
	if ax < minx {minx = ax}
	if ay > maxy {maxy = ay}
	if ay < miny {miny = ay}
}

//Define average position
var avgx = (maxx + minx) / 2
var avgy = (maxy + miny) / 2

xdist = 110; ydist = xdist;
x -= round(xdist * avgx)
y -= round(ydist * avgy)

xoff = x; yoff = y; //Influences drawing the map

connections = [];

//Make connections for each node
for(var z = 1; z != amount+1; z += 1)
{
	var connectionCount = 0;
	
	//Perpendicular
	for(var i = 1; i != min(w,h)-1; i += 1)
	{
		var connectionsAllowed = [true, true, false, false];
		connectionsAllowed = connections_search(network, z, i, connectionsAllowed);
		
		var connectionCount = 0;
		for(var c = 0; c != 2; c += 1)
		{
			if connectionsAllowed[c] = false
				connectionCount += 1;
		}
		
		if connectionCount >= 2 {break; }
	}
	
	//Diagonally
	for(var i = 1; i != min(w,h)-1; i += 1)
	{
		var connectionsAllowed = [false, false, true, true];
		connectionsAllowed = connections_search(network, z, i, connectionsAllowed);
		
		var connectionCount = 0;
		for(var c = 2; c != 4; c += 1)
		{
			if connectionsAllowed[c] = false
				connectionCount += 1;
		}
		
		if connectionCount >= 2 {break; }
	}
}
console_log("Connections: ",array_length_1d(connections));

for(var i = 0; i != array_length_1d(nodes[1].localconnections); i += 1)
{
	connection_reveal(nodes[1].localconnections[i]);
}

//Initialize shop prices
itemlimit = 50;
items = [];
item_define(0,"Fluid",10,20);
item_define(1,"Shards",20,40);
item_define(2,"Cards",20,60);
item_define(3,"Food",30,60);
item_define(4,"Chips",40,80);
item_define(5,"Bulbs",50,99);
console_log(items,"|")

inventory = [0,0,0,0,0,0];
transferred = [0,0,0,0,0,0];

for(var z = 1; z != amount+1; z += 1)
{
	var node = nodes[z];
	for(var i = 0; i != array_height_2d(items); i += 1)
	{
		node.stock[i] = choose(0,0,0,5,10,20)
		
		var base = items[i,1]; var maxi = items[i,2]; var diffitems = items[i,2] - items[i,1];
		
		var connectcount = array_length_1d(node.localconnections);
		var diff = 0;
		switch connectcount
		{
			case 1: diff = 0.25 * diffitems; break;
			case 2: diff = 0.20 * diffitems; break;
			case 3: diff = 0.15 * diffitems; break;
			case 4: diff = 0.1 * diffitems; break;
			case 5: diff = 0.075 * diffitems; break;
			case 6: diff = 0.05 * diffitems; break;
			case 7: diff = 0.025 * diffitems; break;
			default: diff = 0;
		}
		
		base += diff; maxi -= diff;
		
		node.prices[i] = irandom_range(base,maxi)
	}
}

currentnode = 1;

globalvar time; time = 0;
globalvar limit; limit = 600;

globalvar money; money = 500;
globalvar goal; goal = 3499;

hoverednode = -1;
hovereditem = -1;
inventorynettohovered = false;
inventoryamounthovered = false;
//inventorynetworth = 0;

blink = false;
alarm[0] = 1;

heldnode = noone;
heldtimer = 0;
heldtimermax = 20;

buildup = 0;
showseed = false;

gpu_set_alphatestenable(true)

chargesound = noone;

turn = 0;
globalvar netvaluegraph; netvaluegraph[0] = money;

you = "YOU";

cancheat = false;

global.winsprite = noone;

rightoffset = room_width - 340;
bottomoffset = room_height - 180