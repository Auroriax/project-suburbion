{
    "id": "dd8dd888-5976-4354-84b9-91d0735215a7",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_metroNetwork",
    "eventList": [
        {
            "id": "02cd4b49-dd53-44fa-9694-31cb35350504",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "dd8dd888-5976-4354-84b9-91d0735215a7"
        },
        {
            "id": "65cab0f9-d935-4fad-822e-b66ac00554df",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "dd8dd888-5976-4354-84b9-91d0735215a7"
        },
        {
            "id": "0d6b001a-4a35-42a4-94f4-c384f1a63cf1",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 116,
            "eventtype": 9,
            "m_owner": "dd8dd888-5976-4354-84b9-91d0735215a7"
        },
        {
            "id": "39013db6-87b9-451a-8cdf-f9402e106b8d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 122,
            "eventtype": 9,
            "m_owner": "dd8dd888-5976-4354-84b9-91d0735215a7"
        },
        {
            "id": "ccf92bce-6093-4562-bb56-f20adabd8158",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 73,
            "eventtype": 8,
            "m_owner": "dd8dd888-5976-4354-84b9-91d0735215a7"
        },
        {
            "id": "da6e19fa-34d3-4f2c-9fd7-6f1049c86a64",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 75,
            "eventtype": 8,
            "m_owner": "dd8dd888-5976-4354-84b9-91d0735215a7"
        },
        {
            "id": "80b5bcbd-9041-4d7d-b171-67a0689cfea7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 118,
            "eventtype": 9,
            "m_owner": "dd8dd888-5976-4354-84b9-91d0735215a7"
        },
        {
            "id": "e4799d1e-c5bd-42cb-b465-9cf151d0815d",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 119,
            "eventtype": 9,
            "m_owner": "dd8dd888-5976-4354-84b9-91d0735215a7"
        },
        {
            "id": "71c64b62-cc1e-462c-b184-6a08805e8f9a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 2,
            "m_owner": "dd8dd888-5976-4354-84b9-91d0735215a7"
        },
        {
            "id": "a0012a42-7239-42da-bfb2-f088ba1e0842",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 3,
            "m_owner": "dd8dd888-5976-4354-84b9-91d0735215a7"
        },
        {
            "id": "1762fdfd-a0ad-4eeb-981f-24409a02e545",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 114,
            "eventtype": 9,
            "m_owner": "dd8dd888-5976-4354-84b9-91d0735215a7"
        },
        {
            "id": "3404e58f-9e58-4ce6-88c1-7e77c0dc7af6",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 113,
            "eventtype": 9,
            "m_owner": "dd8dd888-5976-4354-84b9-91d0735215a7"
        },
        {
            "id": "3c369d57-a2b9-423d-94ba-e1e62012d148",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 120,
            "eventtype": 9,
            "m_owner": "dd8dd888-5976-4354-84b9-91d0735215a7"
        },
        {
            "id": "9e8dadb0-2cd9-467b-ad52-8e3da833e948",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 77,
            "eventtype": 9,
            "m_owner": "dd8dd888-5976-4354-84b9-91d0735215a7"
        },
        {
            "id": "63a6dcfc-a850-4ad9-ae26-68ee7bea2fee",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 121,
            "eventtype": 9,
            "m_owner": "dd8dd888-5976-4354-84b9-91d0735215a7"
        },
        {
            "id": "bd067a2b-0a15-415d-bb07-39ae28ce564a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 123,
            "eventtype": 9,
            "m_owner": "dd8dd888-5976-4354-84b9-91d0735215a7"
        },
        {
            "id": "ac19582f-f093-4e27-9506-e87f1bb50504",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 50,
            "eventtype": 9,
            "m_owner": "dd8dd888-5976-4354-84b9-91d0735215a7"
        },
        {
            "id": "a2ff9d83-086b-4ccc-8851-48d9a20dbbf7",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 81,
            "eventtype": 9,
            "m_owner": "dd8dd888-5976-4354-84b9-91d0735215a7"
        },
        {
            "id": "bd140ea3-1d01-4520-af0c-5c304e63e03a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 54,
            "eventtype": 6,
            "m_owner": "dd8dd888-5976-4354-84b9-91d0735215a7"
        },
        {
            "id": "6832f716-20db-40dd-84b3-920aa0e5bdd4",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 51,
            "eventtype": 9,
            "m_owner": "dd8dd888-5976-4354-84b9-91d0735215a7"
        },
        {
            "id": "e8eb4266-fef7-414d-aab5-496a7001f04b",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 48,
            "eventtype": 9,
            "m_owner": "dd8dd888-5976-4354-84b9-91d0735215a7"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "00000000-0000-0000-0000-000000000000",
    "visible": true
}