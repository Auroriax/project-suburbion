/// @description QQQ

if mouse_x >= rightoffset && mouse_y >= bottomoffset {exit;}

var ins = instance_position(mouse_x,mouse_y,o_pencilmark)
console_log("Ins: ",ins)
if ins != noone 
{
	instance_destroy(ins)
}
else
{
	if instance_number(o_pencilmark) <= 20
		instance_create_depth(mouse_x,mouse_y,-50,o_pencilmark);
}