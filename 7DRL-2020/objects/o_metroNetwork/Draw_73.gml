/// @description Draw tooltip & UI

hovereditem = -1;

draw_set_alpha(0.5);
draw_set_color(c_dkgray);
draw_rectangle( rightoffset, bottomoffset, room_width, room_height, false);
draw_set_color(c_red);
draw_rectangle(rightoffset - 5, bottomoffset - 5, room_width + 5, room_height + 5, true)
draw_set_alpha(1);

//Draw other interface bits
draw_set_color(c_white);
if (time / limit) > 0.9 {draw_set_color(c_red)}
draw_set_halign(2);
draw_set_font(f_bodytextbold);
draw_text(room_width - 220,room_height * 0.965, "Time: "+ string(limit-time)+" min.*");
//Tooltip for this generated at end of function

draw_set_color(c_white);
draw_text(room_width - 10,room_height * 0.965,"Cash: €"+string(money) + " / " + string(goal) +"*");

//Buyer/inventory
var horoff = room_height * 0.73;
draw_set_halign(1);
draw_text(room_width-295,horoff-5 ,"Station");
draw_line(room_width-340,horoff+15,room_width-250,horoff+15)
draw_text(room_width-65,horoff-5,"Inventory");
draw_line(room_width-120,horoff+15,room_width-10,horoff+15)

var itemcount = 0;
var stationnetworth = 0;
var inventorynetworth = 0;
var stationitemcount = 0;

var half = (room_width - rightoffset) * 0.48;
var centeroff = -18;

//Count up stats for items
for(var i = 0; i != array_height_2d(items); i += 1) {
	var curnode = nodes[currentnode];
	
	itemcount += inventory[i];
}

//Draw items
for(var i = 0; i != array_height_2d(items); i += 1)
{
	var curnode = nodes[currentnode];

	if point_in_rectangle(mouse_x,mouse_y,rightoffset, horoff + (20 * (i+1)), room_width, horoff + (20 * (i+2))) {
		hovereditem = i;
		draw_set_color(c_white); draw_set_alpha(0.25);
		draw_rectangle(rightoffset, horoff + (20 * (i+1)), room_width-1, horoff + (20 * (i+2)),false);
		draw_set_alpha(1);
		
		var amount = 1;
		if keyboard_check(vk_control) {amount = 5}
		if keyboard_check(vk_shift) {amount = itemlimit}
		
		if point_inrectangle(mouse_x,mouse_y,rightoffset, horoff + (20 * (i+1)), rightoffset+half+centeroff, horoff + (20 * (i+2)),true) {
			//draw_rectangle(rightoffset, horoff + (20 * (i+1)), rightoffset + half + centeroff, horoff + (20 * (i+2)),true);
			draw_set_color(c_red); draw_set_font(f_bodytextbold); draw_set_halign(0);
			var txt = "Buy ×"+string(amount);
			draw_textoutline(room_width-350,horoff + (20 * (i)),txt,c_black)
			var wx = draw_text_get_width(room_width-350,horoff + (20 * (i)),txt) + 10;
			
			draw_set_font(f_bodytextsmaller);
			var txt = "+[Ctrl] ×5, +[Shift] ALL"
			draw_textoutline(room_width-350  + wx,horoff + 2 + (20 * (i)),txt,c_black)
			draw_text(room_width-350  + wx,horoff + 2 + (20 * (i)),txt)
			
			if mouse_check_button_pressed(mb_left) { 
				item_buy(i,amount,curnode,itemcount)
			}

		}
		else if point_inrectangle(mouse_x,mouse_y,rightoffset + half - centeroff, horoff + (20 * (i+1)), room_width, horoff + (20 * (i+2)),true) {
			//draw_rectangle(rightoffset + half - centeroff, horoff + (20 * (i+1)), room_width - 1, horoff + (20 * (i+2)),true);
			draw_set_color(c_red); draw_set_halign(2); draw_set_font(f_bodytextbold)
			draw_textoutline(room_width-10,horoff + (20 * (i)),"Sell ×"+string(amount),c_black)
			var wx = draw_text_get_width(room_width-10,horoff + (20 * (i)),"Sell ×"+string(amount)) + 10;
			
			draw_set_font(f_bodytextsmaller);
			var txt = "+[Ctrl] ×5, +[Shift] ALL"
			draw_textoutline(room_width-10 - wx,horoff + 2 + (20 * (i)),txt,c_black)
			draw_text(room_width-10 - wx,horoff + 2 + (20 * (i)),txt)
			
			if mouse_check_button_pressed(mb_left) { 
				if !item_sell(i,amount,curnode) {instance_create_depth(mouse_x,mouse_y,-100,o_nope)}
			}
		}
	}
	
	if even(i)
		draw_set_color(c_white);
	else
		draw_set_color(c_ltgray);
	
	draw_set_font(f_bodytextbold); draw_set_halign(2)
	draw_text_get_width(room_width-350,horoff + (20 * (i+1)),items[i,0])
	var pieoff = 72
	draw_pie(room_width-350-pieoff,horoff + 20 * (i+1)+10,curnode.prices[i]-items[i,1],items[i,2]-items[i,1],c_red,8,1,90)
	
	if even(i)
		draw_set_color(c_white);
	else
		draw_set_color(c_ltgray);
	
	//STATION STOCK
	if (curnode.stock[i] == 0)
		draw_set_alpha(0.25);
	else
		draw_set_alpha(1);

	draw_set_font(f_bodytext); draw_set_halign(2)
	draw_text(room_width-285,horoff + (20 * (i+1)),euro(curnode.stock[i] * curnode.prices[i]))
	
	draw_set_halign(2)
	draw_text(room_width-245,horoff + (20 * (i+1)),itemamount(curnode.stock[i]))
	
	draw_set_alpha(1);
	
	//TRANSFERRING
	draw_set_halign(2); draw_set_font(f_bodytextbold)
	var txt = "+";
	if transferred[i] > 0
		txt += string(transferred[i])
	draw_text(room_width-208,horoff + (20 * (i+1)),txt)
	
	draw_set_halign(2); draw_set_font(f_bodytextbold)
	draw_text(room_width-160,horoff + (20 * (i+1)),"€"+string(curnode.prices[i]))
	
	draw_set_halign(0); draw_set_font(f_bodytextbold)
	var txt = "-";
	if transferred[i] < 0
		txt += string(abs(transferred[i]))
	draw_text(room_width-140,horoff + (20 * (i+1)),txt)
	
	//PLAYER INVENTORY
	if (inventory[i] == 0)
		draw_set_alpha(0.25);
	else
		draw_set_alpha(1);
	
	draw_set_font(f_bodytext); draw_set_halign(2);
	draw_text(room_width-80,horoff + (20 * (i+1)), itemamount(inventory[i]))
	
	draw_text(room_width-10,horoff + (20 * (i+1)), euro(inventory[i] * curnode.prices[i]))
	draw_set_halign(0);
	draw_set_alpha(1);
}

//Count up stats for items
for(var i = 0; i != array_height_2d(items); i += 1) {
	var curnode = nodes[currentnode];
	
	//itemcount += inventory[i];
	stationnetworth += curnode.stock[i] * curnode.prices[i];
	stationitemcount += curnode.stock[i];
	inventorynetworth += inventory[i] * curnode.prices[i];
}

i += 0.25;

//Station cummulatives
draw_set_font(f_bodytext); draw_set_halign(2)
draw_text(room_width-285,horoff + (20 * (i+1)),euro(stationnetworth))
	
draw_set_halign(2)
draw_text(room_width-245,horoff + (20 * (i+1)),itemamount(stationitemcount))

//Minute increased
draw_set_halign(2);
var transfer = 0;
for(var j = 0; j != array_length_1d(transferred); j += 1)
{
	if transferred[j] >= 0
		transfer -= transferred[j];
}
txt = string(transfer) + " min. *"
draw_text(room_width-160,horoff + (20 * (i+1)),txt);

draw_set_halign(2);
//Item count & limit
if (itemcount >= itemlimit)
	draw_set_color(c_red)
else
	draw_set_color(c_white);

draw_text(room_width-80,horoff + (20 * (i+1)),string(itemcount) + "/" + string(itemlimit)+"*");

//Net value
draw_set_color(c_white);
draw_text(room_width-10,horoff + (20 * (i+1)),euro(inventorynetworth+money)+"*");

if itemcount == 0 && hovereditem == -1
{
	draw_set_color(c_red)
	if blink {draw_set_color(c_ltgray)}
	draw_set_font(f_bodytextbold)
	draw_textoutline(room_width-10,horoff + (20 * (3.5)),"Buy something!",c_black)
	draw_text(room_width-10,horoff + (20 * (3.5)),"Buy something!");
}

draw_set_color(c_black)

//NODE TOOLTIP LOGIC
hoverednode = -1;

for(var i = 0; i != w; i += 1)
{
	for(var j = 0; j != h; j += 1)
	{
		var xx = xoff + xdist * i;
		var yy = yoff + ydist * j;
		
		var index = network[i,j]
		
		if (index >= 0 && nodes[index].revealed)
		{
			var offset = 60;
			if (mouse_x < rightoffset || mouse_y < bottomoffset)  && point_in_rectangle(mouse_x,mouse_y,xx - offset, yy - offset, xx + offset, yy + offset) && index > 0
			{
				//Tooltip
				draw_set_halign(0);
				var border = 20;
				var tx = mouse_x + border; 
				var ty = mouse_y + border; 
				var width = 200; 
				var height = 310;
				if (tx + width >= room_width) {tx -= width + border * 2}
				if (ty + height >= room_height){ty -= height + border * 2}
				var c = c_gray; draw_set_alpha(0.95);
				draw_rectangle_color(tx,ty,tx+width,ty+height,c,c,c,c,false)
				var c = c_white; draw_set_alpha(1);
				draw_rectangle_color(tx-2,ty - 2,tx+width + 2,ty+height + 2,c,c,c,c,true);
				var node = nodes[index];
			
				var dx = tx + border; var dy = ty + border;
				draw_set_font(f_bodytextbold)
				dy += draw_text_get_height(dx,dy,node.name) + 5;
				draw_set_font(f_bodytextsmaller)
				dy += draw_text_get_height(dx,dy,node.poi,-1, width - border * 2) + 15;
				draw_set_font(f_bodytext)
				if (!node.visited)
				{
					dy += draw_text_get_height(dx,dy,"You can only see item prices for stations you've visited once.",-1, width - border * 2) + 20;
					
					var foundConnections = 0;
					var foundNodes = 0;
					for(var n = 0; n != array_length_1d(node.localconnections); n += 1)
					{
						var connection = connections[ node.localconnections[n] ]
						if !nodes[connection.stationin].revealed
							foundNodes += 1;
							
						if !nodes[connection.stationout].revealed
							foundNodes += 1;
						
						if (connection.revealed == false)
						{
							foundConnections += 1;
						}
					}
					
					draw_set_font(f_bodytextbold)
					draw_text_get_height(dx,dy,"Go here to reveal "+string(foundConnections)+" connection(s) and "+string(foundNodes)+" station(s).",-1, width - border * 2);
				}
				else
				{
					var totalitems = 0; var totalprice = 0;
					for(var z = 0; z != array_height_2d(items); z += 1)
					{
						var tw = width * 0.6;
						draw_set_color(c_red)
						draw_pie(dx+width * 0.33,dy+10,node.prices[z]-items[z,1],items[z,2]-items[z,1],c_red,8,1,90)
						
						if even(z)
							draw_set_color(c_black);
						else
							draw_set_color($222222);
					
						var txt = items[z,0];
						draw_set_font(f_bodytextbold)
						draw_text_get_width(dx,dy,txt,-1, width - border * 2)
					
						draw_set_font(f_bodytext);
						draw_set_halign(2);
						var tw = width * 0.55;
						var txt = "€" + string(node.prices[z])
						draw_text_get_height(dx+tw,dy,txt,-1, width - border * 2);
					
						if index != currentnode
						{
						if even(z)
							draw_set_color(c_black);
						else
							draw_set_color($222222);
							draw_set_font(f_bodytextsmaller)
							var diff = node.prices[z] - nodes[currentnode].prices[z];
							if diff > 0 {var txt = "▲ +€" + string(diff); draw_set_color(c_red)};
							else if diff < 0 {var txt = "▼ -€" + string(abs(diff))};
							else {txt = "="}
							draw_text(dx+tw,dy-12,txt);
							draw_set_font(f_bodytext);
						}
						
						if even(z)
							draw_set_color(c_black);
						else
							draw_set_color($222222);

						var tw = width * 0.625;
						draw_set_halign(0);
						if node.stock[z] > 0
							{txt = "×" +string(node.stock[z]);}
						else
							{txt = "None"; draw_set_alpha(0.25)}
						dy += draw_text_get_height(dx+tw,dy,txt,-1, width - border * 2) + 10;

						draw_set_font(f_bodytextsmaller)
						if inventory[z] > 0
							{draw_set_alpha(1)}
						else
							{draw_set_alpha(0.25)}
						draw_text(dx+tw,dy-42,"Have: "+string(inventory[z]));
						draw_set_alpha(1)
					
						totalitems += node.stock[z];
						totalprice += node.stock[z] * node.prices[z];
					}
				
					dy += 5;
					draw_set_color(c_black);
					draw_set_font(f_bodytextbold);
					draw_set_halign(2);
					var tw = width * 0.55;
					var txt = "Total: €" +string(totalprice)
					draw_textoutline(dx+tw,dy-1,txt,c_white, width - border * 2)
					draw_text_get_height(dx+tw,dy,txt,-1, width - border * 2);

					var tw = width * 0.6;
					draw_set_halign(0);
					txt = "×" +string(totalitems);
					dy += draw_text_get_height(dx+tw,dy,txt,-1, width - border * 2) + 5;
				
				}
			
				//console_log("Hoverednode now: ",index)
				hoverednode = index;
			}
		}
	}
}

draw_set_color(c_white); draw_set_halign(0); draw_set_valign(2); draw_set_font(f_bodytext)
draw_text(10,room_height - 20, "Project");
draw_set_font(f_bodytextbold)
draw_text(65,room_height - 20, "SUBURBION*")
draw_set_font(f_bodytextsmaller)
draw_text(10,room_height - 5, "by Tom Hermans (@Auroriax)")

if hoverednode == -1
{
	hoverednode = currentnode;
}

draw_set_halign(0);
if point_inrectangle(mouse_x,mouse_y,rightoffset, room_height * 0.965, rightoffset + 150, room_height * 0.965+20, true)
{
	draw_tooltip(20,300,350,"Remaining Time","Decreases with:\n• Each item you buy on a station\nAnd when transferring:\n• Time until the train leaves (number before the + on the map)\n• Travel time (number after the + on the map)\n\nIf transferring stations causes you to run out of time, you lose the game.")
}

if point_inrectangle(mouse_x,mouse_y,room_width - 180, room_height * 0.965, room_width, room_height * 0.965 + 20, true)
{
	draw_tooltip(20,200,250,"Ready Cash","Used to buy items. Buy low and sell high to obtain €"+string(goal)+" and win.\n\nYou currently have "+string(round(money/goal*100))+"% of what you need to win.")
}

var ttheight = 50;

if point_inrectangle(mouse_x,mouse_y,room_width-240,room_height - ttheight, room_width-160, room_height - ttheight + 20, true)
{
	draw_tooltip(20,200,310,"Trading time","Each time you buy an item, one minute passes, affecting your connections.\nUndo by selling back items you bought before leaving a station.\nSelling items you brought from other stations does NOT affect time.")
}

inventoryamounthovered = false;
if point_inrectangle(mouse_x,mouse_y,room_width-150,room_height - ttheight, room_width-80, room_height - ttheight + 20, true)
{
	inventoryamounthovered = true;
	draw_tooltip(20,200,200,"Inventory limit","If you reach have "+string(itemlimit)+" items total, you can't buy any more.\nThe map is now showing item counts for all revealed stations.")
}

inventorynettohovered = false;
if point_inrectangle(mouse_x,mouse_y,room_width-70,room_height - ttheight, room_width-10, room_height - ttheight + 20, true)
{
	inventorynettohovered = true;
	draw_tooltip_graph(20,300,450,"Netto value","The cumulative value of all your owned items (€"+string(inventorynetworth)+"), plus ready cash.\nTransferring stations will change net value based on item prices, see the map.\nThe graph below shows how your net value changed over the last 10 stations you visited.")
}

cancheat = false;
if point_inrectangle(mouse_x,mouse_y,5,room_height - 40, 190, room_height, true)
{
	if mouse_check_button(mb_right)
	{
		cancheat = true;
		draw_tooltip(20,300,520,"Cheats","Keep holding the right mouse button here and press:\n\nM: Toggle music.\n2: Change 'YOU' into an @ sign.\n\n"+
		"F2: Custom seed\nF3 or 3: Reveal & visit entire underground\nF4 + F5: Restart game\nF9: Adds €1000.\nF10 or 0: Add 1 of each item.\nF12: Adds 30 minutes"+
		"\n\nDesktop only:\nF6: Take screenshot, saved in your local app data\nF7: Save state to file\nF8: Load state from file\nF11: Fullscreen")
	}
	else
		draw_tooltip(20,300,520,"Project SUBURBION","In 2050, all means of traffic (except public transportation) have been outlawed, including cargo vehicles. Nowadays, many traders board the subway every day to travel across the urban area, buying low and selling high, to make ends meet. You need to pay your debt today: can you multiply your investment simply by toying with supply and demand?\n\n"+
	"Game by Tom Hermans (@Auroriax)\nMusic under CC-BY by Meydän: http://freemusicarchive.org/music/ Meydan/Ambient_1860/Elk\nFont: Cooper Hewitt by Chester Jenkins under the SIL Open Font Licese v1.10\nFreesound users: SamsterBirdies, Churd-Tzu, potentjello\nMade for 7DRL 2020\nThank you for playing!")
}