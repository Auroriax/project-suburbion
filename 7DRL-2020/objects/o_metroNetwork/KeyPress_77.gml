/// @description Toggle sound

if audio_is_playing(a_music)
{
	audio_stop_sound(a_music);
}
else
{
	audio_play_sound(a_music,0,true)
}