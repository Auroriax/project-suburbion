/// @description Go to new nodes

buildup += 1
if buildup >= 60
{buildup = 0}

if mouse_check_button(mb_left) && hoverednode != currentnode && is_array(nodes)
{
	if heldnode != hoverednode
	{
		if chargesound != noone
		{
			console_log("Stopped: ",chargesound, ": ",audio_get_name(chargesound))
			audio_sound_gain(chargesound,0,0)
			chargesound = noone;
		}
		heldtimer = 1;
		heldnode = noone;
		
		var node = nodes[hoverednode]
		for(var i = 0; i != array_length_1d(node.localconnections); i += 1)
		{
			var connection = connections[node.localconnections[i]];
			if ((connection.stationin == currentnode && connection.stationout == hoverednode)
			|| connection.stationin == hoverednode && connection.stationout == currentnode)
			{		
				console_log("Found match: ",i)
				chargesound = audio(a_charge,random_range(0.7,0.9),0.3)
				heldnode = hoverednode; break;
			}
		}
		if heldnode == noone && mouse_check_button_pressed(mb_left)
		{
			instance_create_depth(mouse_x,mouse_y,-100,o_nope)
		}
	}
	else
	{
		heldtimer += 1;
		if heldtimer >= heldtimermax
		{
			var traversetime = 0;
			
			var node = nodes[hoverednode]
			for(var i = 0; i != array_length_1d(node.localconnections); i += 1)
			{
				var connection = connections[node.localconnections[i]];
				if ((connection.stationin == currentnode && connection.stationout == hoverednode)
				|| connection.stationin == hoverednode && connection.stationout == currentnode)
				{		
					var timeleft = connection.frequency - (time + connection.start) % connection.frequency;
					if (timeleft == connection.frequency) {timeleft = 0};
					traversetime = timeleft + connection.traveltime
					time += traversetime;
					break;
				}
			}
			
			currentnode = heldnode;
			nodes[currentnode].visited = true;
			
			transferred = [0,0,0,0,0,0];
			
			for(var i = 0; i != array_length_1d(nodes[currentnode].localconnections); i += 1)
			{
				connection_reveal(nodes[currentnode].localconnections[i]);
			}

			instance_create_depth(mouse_x,mouse_y,-100,o_ripple)
			floattext(mouse_x,mouse_y,"- "+string(traversetime)+ " min.");
			chargesound = noone;
			
			if time > limit
				gameover();
			
			turn += 1;
			
			var node = nodes[currentnode]
			netvaluegraph[turn] = money;
			for(var i = 0; i != array_height_2d(items); i += 1)
			{
				netvaluegraph[turn] += node.prices[i] * inventory[i];
			}
		}
	}
}
else
{
	if chargesound != noone
		{
			console_log("Stopped: ",chargesound, ": ",audio_get_name(chargesound))
			audio_sound_gain(chargesound,0,100)
			chargesound = noone;
		}
	heldnode = noone;
	heldtimer = 0;
}