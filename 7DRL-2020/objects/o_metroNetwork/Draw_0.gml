/// @description Draw network

gpu_set_blendenable(true);

//Draw connections

for(var i = 0; i != array_length_1d(connections); i += 1)
{
	connection = connections[i];
	if connection.length == 1
	draw_connection(i);
}

for(var i = 0; i != array_length_1d(connections); i += 1)
{
	connection = connections[i];
	if connection.length != 1
	draw_connection(i);
}

if hoverednode > 0
{
	var highlightnodes = nodes[hoverednode].localconnections;
	for(var i = 0; i != array_length_1d(highlightnodes); i += 1)
	{
		draw_connection(highlightnodes[i]);
	}
}

//Draw nodes
for(var i = 0; i != w; i += 1)
{
	for(var j = 0; j != h; j += 1)
	{
		var xx = xoff + xdist * i;
		var yy = yoff + ydist * j;
		
		var index = network[i,j]
		
		if (index >= 0 && nodes[index].revealed)
		{
			draw_set_color(c_white);
			draw_circle(xx, yy, 15, false);
			draw_set_color(c_black);
			var size = 10;
			if !nodes[index].visited {size = 5}
			draw_circle(xx, yy, size, false)
		
			if (hovereditem != -1)
			{
				draw_set_font(f_bodytextbold)
				draw_set_halign(1); draw_set_valign(1);
				var txt = "€"+ string(nodes[index].prices[hovereditem])
				if !nodes[index].visited {txt = "???"}
				
				if (nodes[index].visited)
				{
					var m = items[hovereditem,1];
					draw_circle_color(xx-1,yy-1,30,c_dkgray,c_gray,false)
					draw_pie(xx,yy,nodes[index].prices[hovereditem] - m,items[hovereditem,2]-m,c_red,30,1,90)
					
					draw_set_font(f_bodytextsmaller)
					var txta = "Stock: ×"+string( nodes[index].stock[hovereditem]);
					if nodes[index].stock[hovereditem] == 0 
					{
						txta = "Sell only"
						draw_textoutline(xx,yy+20, txta,c_black);
						var col = c_white;
						draw_text_color(xx,yy+20,txta,col,col,col,col,1);
					}
					else
					{
						draw_textlabel(xx,yy+20, txta,c_black,c_white);
					}
				}
				
				draw_set_halign(1); draw_set_valign(1);
				draw_set_font(f_bodytextbold)
				draw_set_color(c_black)
				draw_textoutline(xx,yy,txt,c_white);
				draw_text(xx,yy,txt);
				
				draw_set_halign(0); draw_set_valign(0);
			}
			if (index == currentnode)
			{
				draw_set_font(f_bodytextbold)
				draw_set_halign(1); draw_set_valign(1);
				draw_set_color(c_red);
				var ytextoff = 0;
				if (hovereditem != -1)
					var ytextoff = -20;
				draw_textoutline(xx,yy+ytextoff,you,c_black);
				draw_text(xx,yy+ytextoff,you);
				draw_set_halign(0); draw_set_valign(0);
			}
			else if (index == heldnode)
			{
				draw_pie(xx,yy, heldtimer, heldtimermax,c_red,25,0.75 + (0.25 + (heldtimer/heldtimermax)), 90)
			}
			else if inventorynettohovered && nodes[index].visited
			{
				var netvalueonstation = money;
				for(var u = 0; u != array_length_1d(inventory); u += 1)
				{
					netvalueonstation += nodes[index].prices[u] * inventory[u];
				}
				
				var netvalueoncurrentstation = money;
				for(var u = 0; u != array_length_1d(inventory); u += 1)
				{
					netvalueoncurrentstation += nodes[currentnode].prices[u] * inventory[u];
				}
				
				draw_set_color(c_white);
				draw_set_font(f_bodytextbold); draw_set_halign(1); draw_set_valign(1);
				var txt = "Inv. Netto\n€"+string(netvalueonstation);
				draw_textlabel(xx,yy,txt,c_black,c_white)
				
				draw_set_font(f_bodytextsmaller); draw_set_halign(1); draw_set_valign(1);
				var diff = netvalueonstation - netvalueoncurrentstation;
				txt = "";
				if diff > 0 {txt = "▲ +€"+string(diff); draw_set_color(c_red)}
				else if diff < 0 {txt = "▼ -€"+string(abs(diff))}
				draw_textoutline(xx,yy-30,txt,c_black);
				draw_text(xx,yy-30,txt)
				
				draw_set_halign(0); draw_set_valign(0);
			}
			else if inventoryamounthovered && nodes[index].visited {
				var stationitemcount = 0;
				for(var u = 0; u != array_length_1d(inventory); u += 1)
				{
					stationitemcount += nodes[index].stock[u];
				}
				
				draw_set_color(c_white);
				draw_set_font(f_bodytextbold); draw_set_halign(1); draw_set_valign(1);
				var txt = "×"+string(stationitemcount);
				draw_textoutline(xx,yy,txt,c_black)
				draw_text(xx,yy,txt)
				draw_set_valign(0);
			}
		}
	}
}