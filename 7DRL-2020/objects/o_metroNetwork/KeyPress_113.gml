/// @description Input seed

if (!cancheat) {exit}

var int = get_integer("Please insert your preferred random seed and click OK to restart the game.",seed)

if int != seed && int != 0
{
	//ds_grid_destroy(network);
	random_set_seed(int);
	audio_stop_all();
	room_restart();
}