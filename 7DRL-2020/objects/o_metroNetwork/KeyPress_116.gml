/// @description Restart game

if keyboard_check(vk_f4)
{
audio_stop_all()
randomize();
room_restart()
}